#ifndef SOCKETDATAGRAMA_H_
#define SOCKETDATAGRAMA_H_

#include "PaqueteDatagrama.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <vector>
#include <string>
#include <list>


using namespace std;

class SocketDatagrama {
	public:
		vector<string> arrdirip;
		list<string> arrd2;
		struct sockaddr_in direccionLocal;
		struct sockaddr_in direccionForanea;
		//struct timeval tiempo_time_out;
		struct itimerval tiempo_time_out;
		struct sigaction act;
		sigset_t mask;
		bool timeout;
		struct timespec tiempox,tiempoy;
		long double srtt,rttvar,delta;//temporizador
		int s;
		bool llego;
	public:
	SocketDatagrama();
	SocketDatagrama(int);
	int recibe(PaqueteDatagrama & p);
	int envia(PaqueteDatagrama & p);
	//Metodos para temporizador
	void setTimeout(time_t segundos,useconds_t microsegundos);
	void unsetTimeout();
	void recibeTimeout(PaqueteDatagrama &p);
	//Metodos para broadcast
	void setBroadcast();
};

#endif
