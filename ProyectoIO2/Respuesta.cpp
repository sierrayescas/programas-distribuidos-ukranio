#include "PaqueteDatagrama.h"
#include "SocketDatagrama.h"
#include "Respuesta.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <stdlib.h>
using namespace std;

Respuesta::Respuesta(int puerto){
	socketLocal=new SocketDatagrama(puerto);
}

Mensaje Respuesta::getRequest(){
	unsigned int lon = sizeof(Mensaje);
	PaqueteDatagrama rec(lon);
	socketLocal->recibe(&rec);
	Mensaje *datos = (Mensaje*)malloc(lon);
	memcpy(datos, rec.obtieneDatos(), rec.obtieneLongitud());

//	printf("%f, %f\n", datos->x, datos->y);
	//datos->puerto = rec.obtienePuerto();
	//strcpy(datos->IP,rec.obtieneDireccion());
	
	return *datos;
}

/*void Respuesta::sendReply(char *datos, char *ipCliente, int PuertoCliente, int requestId){
	mensaje *mensaje1=(mensaje*)malloc(sizeof(mensaje));
	mensaje1->messageType=1;
	mensaje1->requestId=requestId;
	strcpy(mensaje1->IP,ipCliente);
 	mensaje1->puerto=PuertoCliente;
 	mensaje1->operationId=1;
 	memcpy(mensaje1->arguments,datos,sizeof(datos));

 	PaqueteDatagrama paquete((char *)mensaje1,sizeof(mensaje),ipCliente,PuertoCliente);
 	socketLocal->envia(paquete);
}

void Respuesta::sendReplyBroadcast(char *datos, char *ipCliente, int PuertoCliente, int requestId){
	mensaje *mensaje1=(mensaje*)malloc(sizeof(mensaje));
	mensaje1->messageType=1;
	mensaje1->requestId=requestId;
	strcpy(mensaje1->IP,ipCliente);
 	mensaje1->puerto=PuertoCliente;
 	mensaje1->operationId=1;
 	memcpy(mensaje1->arguments,datos,120*sizeof(int[2]));

 	PaqueteDatagrama paquete((char *)mensaje1,sizeof(mensaje),ipCliente,PuertoCliente);
 	socketLocal->setBroadcast();
 	socketLocal->envia(paquete);
 	socketLocal->unsetBroadcast();
}*/