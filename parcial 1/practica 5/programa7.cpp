#include <iostream>
#include <vector>
using namespace std;

class Coordenada{
	private:
		double x;
		double y;
 	public:
		Coordenada(double = 0, double = 0);
		double obtenerX();
		double obtenerY();
};

class Rectangulo{
 	private:
		Coordenada superiorIzq;
		Coordenada inferiorDer;
 	public:
		Rectangulo();
		Rectangulo(double xSupIzq, double ySupIzq, double xInfDer, double yInfDer);
		void imprimeEsq();
		Coordenada obtieneSupIzq();
		Coordenada obtieneInfDer();
};

class PoligonoIrreg{
 	private:
		vector<Coordenada> vec;
		static int contadorchido;
 	public:
		PoligonoIrreg();
		void anadeVertice(Coordenada);
		void imprimeVertices();
		static int getcontadorchido();
};

int PoligonoIrreg::contadorchido = 0;

int main(){
	vector <PoligonoIrreg> vpi;
	PoligonoIrreg pi;
	int i, j, vertices,nodos;
	for(i = 0; i < 10000; i++){ //numero de poligonos
		vertices = rand()%10;
		for(j = 0; j < vertices; j++){ //numero de vertices
			pi.anadeVertice(Coordenada(1,1));
		}
		vpi.push_back(pi);
	}
	
	nodos = pi.getcontadorchido();
cout<<"nodos totales: "<<nodos<<endl;

	return 0;
}

PoligonoIrreg::PoligonoIrreg(){
	contadorchido=0;
}

void PoligonoIrreg::anadeVertice(Coordenada coo){
	vec.push_back(coo);
	contadorchido++;
}
void PoligonoIrreg::imprimeVertices(){
	cout << "Los vertices son: ";
	int i;
	for(i = 0; i < vec.size(); i++){
		cout << " (" << vec[i].obtenerX() << "," << vec[i].obtenerY() << ")";
	}
	cout << endl;
}
int PoligonoIrreg::getcontadorchido(){
	return contadorchido;
}


Coordenada::Coordenada(double xx, double yy) : x(xx), y(yy){
}
double Coordenada::obtenerX(){
	return x;
}
double Coordenada::obtenerY(){
	return y;
}


Rectangulo::Rectangulo() : superiorIzq(0,0), inferiorDer(0,0){
}
Rectangulo::Rectangulo(double xSupIzq, double ySupIzq, double xInfDer, double yInfDer):superiorIzq(xSupIzq, ySupIzq), inferiorDer(xInfDer, yInfDer){
}
void Rectangulo::imprimeEsq(){
	cout << "Para la esquina superior izquierda.\n";
	cout << "x = " << superiorIzq.obtenerX() << " y = " << superiorIzq.obtenerY() << endl;
	cout << "Para la esquina inferior derecha.\n";
	cout << "x = " << inferiorDer.obtenerX() << " y = " << inferiorDer.obtenerY() << endl;
}
Coordenada Rectangulo::obtieneSupIzq(){
	return superiorIzq;
}
Coordenada Rectangulo::obtieneInfDer(){
	return inferiorDer;
}
