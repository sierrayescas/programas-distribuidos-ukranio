import math
def main():
	i = 1 
	seno = 0
	coseno = 0
	tangente = 0
	logaritmo = 0
	raizCuad = 0
	max = 40000000;
	while i < max:
		seno += math.sin(i)
		coseno += math.cos(i)
		tangente += math.tan(i)
		logaritmo += math.log(i)
		raizCuad += math.sqrt(i)
		i += 1

if __name__ == "__main__":
	main()