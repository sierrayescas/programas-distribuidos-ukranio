#include <iostream>
#include <string.h>
#include "PaqueteDatagrama.h"
using namespace std;

PaqueteDatagrama::PaqueteDatagrama(Datos *d, unsigned int l, char *i, int p):longitud(l), puerto(p){
	datos = new Datos;
	memcpy(datos, d, longitud);
	memcpy(ip, i, 16);
}

PaqueteDatagrama::PaqueteDatagrama(unsigned int l): longitud(l){
	datos =new Datos;
}

PaqueteDatagrama::~PaqueteDatagrama() {
}

char * PaqueteDatagrama::obtieneDireccion(){
	return ip;
}

unsigned int PaqueteDatagrama::obtieneLongitud(){
	return longitud;
}

int PaqueteDatagrama::obtienePuerto(){
	return puerto;
}

Datos * PaqueteDatagrama::obtieneDatos(){
	return datos;
}

void PaqueteDatagrama::inicializaPuerto(int p){
	puerto = p;
}

void PaqueteDatagrama::inicializaIp(char *i){
	strcpy(ip, i);
}

void PaqueteDatagrama::inicializaDatos(Datos *d){
	memcpy(datos, d, longitud);
}