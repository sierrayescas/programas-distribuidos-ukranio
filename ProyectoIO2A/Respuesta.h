#ifndef Respuesta_h_
#define Respuesta_h_

#include "SocketDatagrama.h"
#include "mensaje.h"

class Respuesta{
	private:
        SocketDatagrama *socketLocal;
    public:
        Respuesta(int puertoServidor);
        void getRequest(void);
        //void sendReply(char *datos, char *ipCliente, int PuertoCliente, int requestId);
        //void sendReplyBroadcast(char *datos, char *ipCliente, int PuertoCliente, int requestId);
};
#endif