#include <iostream>
#include <vector>
#include<stdlib.h>
using namespace std;

class Coordenada{
	private:
		double x;
		double y;
 	public:
		Coordenada(double = 0, double = 0);
		double obtenerX();
		double obtenerY();
};

class Rectangulo{
 	private:
		Coordenada superiorIzq;
		Coordenada inferiorDer;
 	public:
		Rectangulo();
		Rectangulo(double xSupIzq, double ySupIzq, double xInfDer, double yInfDer);
		void imprimeEsq();
		Coordenada obtieneSupIzq();
		Coordenada obtieneInfDer();
};

class PoligonoIrreg{
 	private:
		vector<Coordenada> vec;
		static int contadorchido;
 	public:
		PoligonoIrreg();
		void anadeVertice(Coordenada,int);
		void imprimeVertices();
		void reserveVertices(int);
		static int getcontadorchido();
};

int PoligonoIrreg::contadorchido = 0;

int main(){
	vector <PoligonoIrreg> vpi;
	vpi.reserve(10000);
	PoligonoIrreg pi;
	int i, j, vertices,nodos;
	for(i = 0; i < 10000; i++){ //numero de poligonos
		vertices = rand()%10;
		//svpi[i].reserveVertices(vertices);
		pi.reserveVertices(vertices);
		for(j = 0; j < vertices; j++){ //numero de vertices
//			cout<<"poligono "<< i <<"Vertice "<<j <<endl;
			pi.anadeVertice(Coordenada(1,1),j);
		}
		vpi[i] = pi;
	}
	nodos = pi.getcontadorchido();
cout<<"nodos totales: "<<nodos<<endl;
	

	return 0;
}

PoligonoIrreg::PoligonoIrreg(){
	contadorchido=0;
}

void PoligonoIrreg::reserveVertices(int a){
	vec.reserve(a);
}

void PoligonoIrreg::anadeVertice(Coordenada coo,int a){
	vec[a]=coo;
	contadorchido++;
}
void PoligonoIrreg::imprimeVertices(){
	cout << "Los vertices son: ";
	int i;
	for(i = 0; i < vec.size(); i++){
		cout << " (" << vec[i].obtenerX() << "," << vec[i].obtenerY() << ")";
	}
	cout << endl;
}
int PoligonoIrreg::getcontadorchido(){
	return contadorchido;
}


Coordenada::Coordenada(double xx, double yy) : x(xx), y(yy){
}
double Coordenada::obtenerX(){
	return x;
}
double Coordenada::obtenerY(){
	return y;
}


Rectangulo::Rectangulo() : superiorIzq(0,0), inferiorDer(0,0){
}
Rectangulo::Rectangulo(double xSupIzq, double ySupIzq, double xInfDer, double yInfDer):superiorIzq(xSupIzq, ySupIzq), inferiorDer(xInfDer, yInfDer){
}
void Rectangulo::imprimeEsq(){
	cout << "Para la esquina superior izquierda.\n";
	cout << "x = " << superiorIzq.obtenerX() << " y = " << superiorIzq.obtenerY() << endl;
	cout << "Para la esquina inferior derecha.\n";
	cout << "x = " << inferiorDer.obtenerX() << " y = " << inferiorDer.obtenerY() << endl;
}
Coordenada Rectangulo::obtieneSupIzq(){
	return superiorIzq;
}
Coordenada Rectangulo::obtieneInfDer(){
	return inferiorDer;
}
