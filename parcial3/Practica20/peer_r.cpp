#include <iostream>
#include "Respuesta.h"
#include <string.h>
#include <stdlib.h>
#include <vector>
using namespace std;
int buscar(int *save,int identificador);
int main(int argc, char* argv[]){
	int *save = (int*)calloc(10000, sizeof(int));
	Respuesta resp(atoi(argv[1]));
	struct mensaje *msj= new struct mensaje;
	char *respuesta=(char*)malloc(4000);
	int nbd = 0, n;
	while(1){
		msj= resp.getRequest();
		if(buscar(save,msj->requestId)){
			cout<<"Identificador de mensaje: "<<msj->requestId<<endl;
			save[msj->requestId] = msj->requestId;
			switch(msj->operationId){
				case 1: //lectura
					cout<<"mensaje cliente: "<<msj->arguments<<endl;
					sprintf(respuesta, "%d", nbd);
				break;
				case 2: //escritura
					cout<<"mensaje cliente: "<<msj->arguments<<endl;
					nbd += atoi(msj->arguments);
					sprintf(respuesta, "%d", nbd);
				break;
				default:
					cout<<"Operacion no encontrada"<<endl;
			}
			if(msj->requestId != 6)
				resp.sendReply(respuesta,msj->IP,msj->puerto);
		}else{
			cout<<"Operacion con ID de mensaje: " <<msj->requestId<<" ya realizada"<<endl;
			sprintf(respuesta, "Operacion realizada anteriormente");
			resp.sendReply(respuesta,msj->IP,msj->puerto);
		}
	}
	
return 0;
}

int buscar(int *save,int identificador){//devuelve 1 si no lo encontro, 0 si lo encontro
	int i;
	for(i=0;i<10000;i++){
		if(save[i] == identificador)
			return 0;
	}
	return 1;
}