#include "Solicitud.h"
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include "mensaje.h"
#include <string.h>
#include <iostream>
using namespace std;
Solicitud::Solicitud(){
	socketlocal = new SocketDatagrama(0);
}
char* Solicitud::doOperation(char *IP, int puerto, int operationId, char *arguments){
	unsigned int lon = sizeof(struct mensaje);
	PaqueteDatagrama env(lon);
	PaqueteDatagrama rec(lon);
	struct mensaje men;
	char msj[sizeof(struct mensaje)];
	int cont = 0;
	env.inicializaPuerto(puerto);
    env.inicializaIp(IP);
	memcpy(&men.arguments,arguments,4000);
	men.messageType=0;
	men.requestId=0;
	memcpy(&men.IP,env.obtieneDireccion(),16);
	men.puerto= env.obtienePuerto();
	men.operationId=operationId;
	memcpy(msj,&men,sizeof(struct mensaje));
	env.inicializaDatos(msj);
	int temp;
	do{
		socketlocal->envia(&env);
		temp = socketlocal->recibeTimeout(&rec, 2, 500000);
		cont++;
	}while(cont < 7 && temp < 0);
	if(temp == -1){
		printf("Hola buenos dias:\n");
		char* lol = (char*)malloc(100*sizeof(char));
		strcpy(lol,"Servidor no disponible, intenta mas tarde baka");
		return lol;
	}else{
		memcpy(&men,rec.obtieneDatos(),sizeof(struct mensaje));
		//cout<<men.arguments<<"Si recive respuesta"<<endl;
		char *respuesta = men.arguments;
		return respuesta;
	}
}