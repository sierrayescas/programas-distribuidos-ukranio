#include <iostream>
#include <string.h>
#include <cmath>
#include <unistd.h>
#include <sys/time.h>
#include <sstream>
#include <string>
#include <math.h>
#include <thread>
#include <time.h> 

#include "Respuesta.h"
//#include "mensaje.h"
#include "gfxModified.h"
#include "Coordenada.h"
#include "Tank.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>

#define puertoServidor 7200
#define puertoPosicion 7201
#define puertoDisparo 7202
using namespace std;

#define ANCHURA 600
#define ALTURA 600
#define PI 3.1415

Respuesta respuesta(puertoServidor);
char ipBroadcast[16];
int posiciones[120][2];
int contador=0;
Mensaje *recibido;
double velocidad;
int wavtoplay=-1;
clock_t tim0,tim1;
float Ttime=0.5;
Mensaje jugador;
//<atomic>(int) nombre(1);
void movPlayer(Tank *t1, bool keys[7],int i);
void updateDegrees(Tank *t1, bool keys[7],int i);
void drawPlayer(Tank *t1);
void InicialSet(Tank *tankes);	
void wavPlays();
/*void Enviar(){
	while(1){
		sleep(1);
		if(contador!=0){
			for(int i=contador;i<120;i++){
				posiciones[i][0]=0;
				posiciones[i][1]=0;
			}
			respuesta.sendReplyBroadcast((char*)posiciones,ipBroadcast,puertoPosicion,1);
			cout<<"Envie las posiciones a "<<ipBroadcast<<" "<<puertoPosicion<<endl;
			contador=0;
		}
	}
}*/

/*
operationId==1: Recibe posicion
operationId==2: Recibe disparo
*/
void Recibir(){
	//struct timeval tiempo;
	int dato[2];
	while(1){		
		//printf("HOla\n");
		jugador =respuesta.getRequest();
		//printf("HOla\n");
	}
}

int main(int argc,char* argv[]){
	int i;
	if(argc!=2){
		cout<<"Ingrese "<<argv[0]<<" ipBroadcast"<<endl;
		exit(-1);
	}
	strcpy(ipBroadcast,argv[1]);
	//thread th1(Enviar),th2(Recibir);
	thread th2(Recibir);
	gfx_open(ANCHURA, ALTURA, "SHIP IO Server");
	gfx_color(0,0,0);
	//Tank t1;//tanke de prueba
	//t1.setPosicion(Coordenada(0, 50));
	velocidad = 44.444*ANCHURA/50000;
	int cont = 0;
	bool keys[7] = {0,0,0,0,0,0,0};
	bool escapeKeyPressed = false; //escape key terminates game while playing
	char str[7];
	//actualizacion, se agrego a la clase tank :v
	Tank tankes[8];
	InicialSet(tankes);
	thread threadR(wavPlays);
	tim0 = clock();
	while(1){
		gfx_clear();
		gfx_clear_color(255, 255, 255);
		



	/*	if (gfx_event_waiting2()) {//no requerido en el server

			gfx_clear();
			escapeKeyPressed = gfx_keyPress(keys);
            if (escapeKeyPressed) return 0;*/

			//movimiento del jugador
			tim1 = clock();//verificacion del disparo
			for (i=0;i<8;i++){
				movPlayer(&tankes[i], keys,i);//da movimiento a un jugador
				updateDegrees(&tankes[i],keys,i);
			}
			////////////////////////////////////////
			/*/ENVIAR TANKE A SERVIDOR
			Mensaje *ms = (Mensaje*)malloc(sizeof(Mensaje));
			ms->requestId = 1; //Identificador del mensaje
		 	ms->operationId = 1; //Identificador de la operación
		 	ms->x = tankes[4].getPosicion().obtenerX();
			ms->y = tankes[4].getPosicion().obtenerY();
			ms->anguloOruga = tankes[4].getAnguloOruga();
			ms->anguloCanon = tankes[4].getAnguloCanon();
			ms->isAlive = true;
			Solicitud sl(puertoLocal);
			sl.sendTank(ip_s, puertoServidor, (char*)ms);
			//FIN ENVIO A SERVIDOR
			//////////////////////////////////////*/
			
			for(i=0;i<8;i++){//actualiza la pocision de los tankes en pantalla
				drawPlayer(&tankes[i]);
			}

			gfx_flush();
			//fin movimiento jugador

			//Actualiza angulos cañones
			/*
			//Imprime en pantalla los angulos
			if(tankes[4].getAnguloCanon() < 0)
				sprintf(str, "%0.2f°",tankes[4].getAnguloCanon() * -1);//cañon jugador
			else
				sprintf(str, "%0.2f°",tankes[4].getAnguloCanon());//cañon jugador
			gfx_text(ANCHURA-38, 10, str);

			if(tankes[0].getAnguloOruga() < 0)
				sprintf(str, "%0.2f°",tankes[4].getAnguloOruga() * -1);//oruga jugador
			else
				sprintf(str, "%0.2f°",tankes[4].getAnguloOruga());//oruga jugador
			gfx_text(ANCHURA-38, 23, str);
			//fin angulos
			*/



		//}

		usleep(10000);
	}
	th2.join();
}

void movPlayer(Tank *t1,bool keys[7],int i){
	double Vx;
	double Vy;
	float angulo = t1->getAnguloOruga();
	float anguloC = t1->getAnguloCanon();
	float aux = angulo*PI/180;
	float auxCanon = anguloC*PI/180;
	if(jugador.whoiam=i){
	float angulo = t1->getAnguloOruga();
	float anguloC = t1->getAnguloCanon();
	double x1,y1,x2,y2,m;//m = pendiente
	t1->setAnguloOruga(jugador.anguloOruga);
	t1->setPosicion(Coordenada(jugador.x,jugador.y));
	if(jugador.operationId==2){
		wavtoplay=1;
			m = tan((double)(auxCanon));
			//cout<<m<<endl;
			x1 = t1->getPosicion().obtenerX();
			y1 = t1->getPosicion().obtenerY();
			x2 = x1 + 10000*cos(auxCanon+aux);
			y2 = y1 + 10000*sin(auxCanon-aux);
			//a^2+b^2=C^2
			gfx_line(x1, y1, x2, y2);
			gfx_flush();
	}
	}
	
}

//360 = 2piRad
void updateDegrees(Tank  *t1,bool keys[7],int i){
	/*Codigo cliente
	float angulo = t1->getAnguloCanon();
	if(keys[5]==1){//k
		if(angulo == 360 )
			angulo = 0;
		else
			angulo = (angulo + 0.5);//%360;
	}
	else if(keys[4]==1){//h
		if(angulo == 0 )
			angulo = 360;
		else
			angulo = (angulo - 0.5);//%360;
	}
	t1->setAnguloCanon(angulo);
	*/
	if(i==jugador.whoiam){
	t1->setAnguloCanon(jugador.anguloCanon);
	}
}

void drawPlayer(Tank *t1){
	int num_pts = 5;
	int posX = t1->getPosicion().obtenerX();
	int posY = t1->getPosicion().obtenerY();
	int angulo = -t1->getAnguloOruga();
	float angle = (float)angulo;
	///*con rotacion de cubo
	XPoint pointsarr[] = {{(short int)(posX+sqrt(40)*cos((angle + 135) *PI/180)), (short int)(posY+sqrt(40)*sin((angle + 135)*PI/180))},
	{(short int)(posX+sqrt(40)*cos((angle - 135) *PI/180)), (short int)(posY+sqrt(40)*sin((angle - 135)*PI/180))},
	{(short int)(posX+sqrt(40)*cos((angle - 45) *PI/180)), (short int)(posY+sqrt(40)*sin((angle - 45)*PI/180))},
	{(short int)(posX+sqrt(40)*cos((angle + 45) *PI/180)), (short int)(posY+sqrt(40)*sin((angle + 45)*PI/180))},
	{(short int)(posX+sqrt(40)*cos((angle + 135) *PI/180)), (short int)(posY+sqrt(40)*sin((angle + 135)*PI/180))}};
	//*/
 	//DIBUJA EL LA LINEA DE CAñON
	float aO = t1->getAnguloOruga();
	float aC = t1->getAnguloCanon();
	float anguloDisparo = (aO+aC);
	double x2 = posX + 10*cos(anguloDisparo*PI/180);
	double y2 = posY + 10*sin((aC-aO)*PI/180);
	gfx_line(posX, posY, x2, y2);
	gfx_polygon(pointsarr, num_pts);
}

void InicialSet(Tank *tankes){
	int i;
	for(i=0;i<8;i++){
		if(i<4){
			//gfx_color(218,20,20);
			tankes[i].setPosicion(Coordenada((i+1)*ANCHURA/5,10));
			//gfx_color(0,0,0);
		}
		else{
			//gfx_color(48,176,19);
			tankes[i].setPosicion(Coordenada((i-3)*ANCHURA/5,ALTURA-10));
		//	gfx_color(0,0,0);
		}
	}
}

void wavPlays(){
	while(1){
		switch(wavtoplay){
			case 1:
				system("aplay disparo.wav");
				wavtoplay=-1;
			break;
		}
	}
}