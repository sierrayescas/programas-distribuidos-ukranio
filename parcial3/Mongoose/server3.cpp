#define MG_ENABLE_HTTP_STREAMING_MULTIPART 1
#include "mongoose.h"
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <thread>
#include <time.h> 
using namespace std;

static const char *s_http_port = "8000";
static struct mg_serve_http_opts s_http_server_opts;
void hiloenvia(char*);
char hilorecive();
SocketDatagrama cliente(7656); //puerto local
char salida[1024]=""; //Ip
clock_t t0;

static void handle_size(struct mg_connection *nc, struct http_message *hm) {
		char query[1024];
		mg_get_http_var(&hm->body, "query", query,sizeof(query));
		stpcpy(query,salida);
		printf("Cadena enviada: %s\n", query);

		mg_send_head(nc,200,strlen(query), "Content-Type: text/plain");
		mg_printf(nc, "%s", query);
}

static void ev_handler(struct mg_connection *nc, int ev, void *p) {
	char query[256];
 	struct http_message *hm = (struct http_message *) p;


	if (ev == MG_EV_HTTP_REQUEST) {
		if (mg_vcmp(&hm->uri, "/search") == 0) { 
			
			mg_get_http_var(&hm->body, "query", query,sizeof(query));
			printf("Cadena introducida: %s\n",query);
			//codigo
			stpcpy(salida,"");
			t0=clock();		
			hiloenvia(query);
			sleep(3);
			//codigo
		    handle_size(nc, hm);  
		}else{
			mg_serve_http(nc, (struct http_message *) p, s_http_server_opts);
		}
	}

}

int main(void) {
	struct mg_mgr mgr;
	struct mg_connection *nc;
	mg_mgr_init(&mgr, NULL);
	cliente.setBroadcast(1);//configurando soket como broadcast
	printf("Starting web server on port %s\n", s_http_port);
	nc = mg_bind(&mgr, s_http_port, ev_handler);
	if (nc == NULL) {
		printf("Failed to create listener\n");
		return 1;
	}
	// Set up HTTP server parameters
	mg_set_protocol_http_websocket(nc);
	s_http_server_opts.document_root = "www"; // Serve current directory
	s_http_server_opts.enable_directory_listing = "yes";
	//codigo
	thread threadR(hilorecive);
	//codigo
	for (;;) {
		mg_mgr_poll(&mgr, 1000);
	}
	mg_mgr_free(&mgr);
	//codigo
	threadR.join();
	//codigo
	return 0;
}


void hiloenvia(char* query){
	
	int num[2];
	num[0] = 7;
	num[1] = 8;
	PaqueteDatagrama env(100);
	char* temp = (char *)malloc(2*sizeof(int));
	memcpy(temp, (char *)num, 2*sizeof(int));
	//char* ip = (char *) malloc(16*sizeof(char));
	//strcpy(ip, "10.100."); 
	env.inicializaDatos(temp);
	env.inicializaPuerto(7200);
	//env.inicializaIp(ip);
	int i, j;
	char* ip2 = (char *) malloc(16*sizeof(char));
			strcpy(ip2, query);
			printf("ip: %s\n", ip2);//imprime ip
			env.inicializaIp(ip2);
			cliente.envia(&env);
			//cliente.recibeTimeout(&rec, 1, 500000);		
}

char hilorecive(){
	PaqueteDatagrama rec(100);
	double time;
	char aux[10];
	while(1){
		cliente.recibe(&rec);
		strcat (salida,"<br>ip: ");
		strcat (salida, rec.obtieneDireccion());
		strcat (salida, ": ");
		clock_t t1 = clock();
		t1+=100;
		printf("\n\n%d\n\n%d\n\n",t0,t1);
		printf("The time was: %f\n", (t1 - t0) / CLOCKS_PER_SEC);
		time = ((double)(t1-t0))/CLOCKS_PER_SEC;
		sprintf(aux,"%lf",time);
		strcat (salida,aux);
		printf("\n********************\n%s\n********************\n",salida);
	}
}