#ifndef PAQUETEDATAGRAMA_H_
#define PAQUETEDATAGRAMA_H_
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>


class PaqueteDatagrama {

private :
char *datos;
char ip[16];
unsigned longitud;
int puerto;

public :
//Datos,longitud datos, direccion IP envio, puerto envio
 PaqueteDatagrama(void *, unsigned , const char *, int);
//Mensaje vacio para recepcion, con logitud determinada
PaqueteDatagrama(unsigned );
//Regra la dir IP de asociada al datagrama
char *obtieneDireccion();
unsigned obtieneLongitud();
int obtienePuerto();
char * obtieneDatos();
void inicializaPuerto(int);
void inicializaIP(char *);
void inicializaDatos(char *);
};

#endif
