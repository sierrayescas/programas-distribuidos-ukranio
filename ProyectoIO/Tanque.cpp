#ifndef TANK_H_
#define TANK_H_
#include "Coordenada.h"
class Tanque{
 	private:
		int rojo;
		int azul;
		int verde;
		Coordenada posicion;
 	public:
		Tanque();
		//Rectangulo(double xSupIzq, double ySupIzq, double xInfDer, double yInfDer);
		//Rectangulo(Coordenada c1, Coordenada c2); 					//AGREGADA POR MI XD
		void setPosicion(Coordenada ps);
		Coordenada getPosicion();
};
#endif