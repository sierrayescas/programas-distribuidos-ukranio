#include <iostream>
#include "Respuesta.h"
#include <string.h>
#include <stdlib.h>
#include <vector>
using namespace std;

int main(int argc, char* argv[]){
	Respuesta resp(atoi(argv[1]));
	struct mensaje *msj= new struct mensaje;
	char *respuesta=(char*)malloc(4000);
	int nbd = 0, n;
	for(;;){
		msj= resp.getRequest();
		switch(msj->operationId){
			case 1: //lectura
				cout<<"mensaje cliente: "<<msj->arguments<<endl;
				n = sprintf(respuesta, "%d", nbd);
			break;
			case 2: //escritura
				cout<<"mensaje cliente: "<<msj->arguments<<endl;
				nbd = atoi(msj->arguments);
				n = sprintf(respuesta, "%d", nbd);
			break;
			default:
				cout<<"Operacion no encontrada"<<endl;
		}
		
		resp.sendReply(respuesta,msj->IP,msj->puerto);
	}
	
return 0;
}