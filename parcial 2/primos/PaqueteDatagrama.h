#ifndef DATA_H_
#define DATA_H_
class Datos{
	public:
	unsigned int numero;
	char estado;
	int inicio;
	int fin;
};
class PaqueteDatagrama {
	public:
	 	PaqueteDatagrama(Datos *, unsigned int, char *, int );
	 	PaqueteDatagrama(unsigned int );
	 	~PaqueteDatagrama();
	 	char *obtieneDireccion();
	 	unsigned int obtieneLongitud();
	 	int obtienePuerto();
	 	Datos * obtieneDatos();
	 	void inicializaPuerto(int);
	 	void inicializaIp(char *);
	 	void inicializaDatos(Datos *);
	private:
	 	Datos *datos; //Almacena los datos
	 	char ip[16]; //Almacena la IP destino
	 	unsigned int longitud; //Almacena la longitude de la cadena de datos
	 	int puerto; //Almacena el puerto destino
};

#endif