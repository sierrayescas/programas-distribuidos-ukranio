#include "sockets/SocketDatagrama.h"
#include "sockets/PaqueteDatagrama.h"
#include <iostream>
#include <cstdlib>
#include <sys/time.h>

using namespace std;

int main() {
  struct timeval temps;
  char *dirip;
  int port;
  
  cout << "Soy el servidor" << endl;
  SocketDatagrama sock = SocketDatagrama(7777);
  
  while(1) {
    PaqueteDatagrama paquet_recu = PaqueteDatagrama(sizeof(int));
    sock.recibeTimeout(paquet_recu);
    gettimeofday(&temps,NULL);
    dirip = paquet_recu.obtieneDireccion();
    port = paquet_recu.obtienePuerto();
    PaqueteDatagrama paquet_envoye = PaqueteDatagrama(
      &temps,sizeof(struct timeval),dirip,port);
    cout << dirip <<":"<< port <<endl;
    sock.envia(paquet_envoye);
  
  cout << "El tiempo es segundos-> " << temps.tv_sec 
    <<"usegundos -> "<<temps.tv_usec << endl; 
  // system("timedatectl");
  }
return 0;
}
