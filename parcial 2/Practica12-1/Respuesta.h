#ifndef RES_H_
#define RES_H_
#include "mensaje.h"
#include "SocketDatagrama.h"
class Respuesta{
public:
 Respuesta(int pl);
 struct mensaje *getRequest(void);
 void sendReply(char *respuesta, char *ipCliente, int puertoCliente);
private:
 SocketDatagrama *socketlocal;
};
#endif