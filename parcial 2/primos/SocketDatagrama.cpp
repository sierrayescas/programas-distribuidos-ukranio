#include <iostream>
#include <string.h>

#include <arpa/inet.h>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
using namespace std;

SocketDatagrama::SocketDatagrama(int puertoVallarta){
	s = socket(AF_INET, SOCK_DGRAM, 0);

	bzero((char *)&direccionLocal, sizeof(direccionLocal));
	bzero((char *)&direccionForanea, sizeof(direccionForanea));
	direccionLocal.sin_family = AF_INET;
	direccionForanea.sin_family = AF_INET;
	direccionLocal.sin_addr.s_addr = INADDR_ANY;
	direccionLocal.sin_port = htons(puertoVallarta);
	bind(s, (struct sockaddr *)&direccionLocal, sizeof(direccionLocal));
}

SocketDatagrama::~SocketDatagrama(){

}

//Recibe un paquete tipo datagrama proveniente de este socket
int SocketDatagrama::recibe(PaqueteDatagrama *p){

		int clilen = sizeof(direccionForanea);
		int *lel = &clilen;
		int temp = recvfrom(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, (struct sockaddr *)&direccionForanea, (socklen_t*)lel);
		p->inicializaIp(inet_ntoa(direccionForanea.sin_addr));
		p->inicializaPuerto(direccionForanea.sin_port);
		return temp;

}

//Envía un paquete tipo datagrama desde este socket
int SocketDatagrama::envia(PaqueteDatagrama *p){
		direccionForanea.sin_addr.s_addr = inet_addr(p->obtieneDireccion());
		//cout<<inet_ntoa(direccionForanea.sin_addr)<<endl;
		direccionForanea.sin_port = htons(p->obtienePuerto());
		//cout<< p->obtienePuerto()<<endl;
		//cout<<direccionForanea.sin_port<<endl;
		return sendto(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, (struct sockaddr *)&direccionForanea, sizeof(direccionForanea));

}