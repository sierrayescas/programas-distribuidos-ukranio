#include <iostream>
#include <string.h>

#include <arpa/inet.h>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
using namespace std;

SocketDatagrama::SocketDatagrama(int puertoVallarta, int flag){
	s = socket(AF_INET, SOCK_DGRAM, 0);

	if(flag == 0){
		/* rellena la dirección del servidor */
		bzero((char *)&direccionLocal, sizeof(direccionLocal));
		direccionLocal.sin_family = AF_INET;
		direccionLocal.sin_addr.s_addr = INADDR_ANY;
		direccionLocal.sin_port = htons(puertoVallarta);
		bind(s, (struct sockaddr *)&direccionLocal, sizeof(direccionLocal));
	}else{
		/* rellena la dirección del servidor */
		bzero((char *)&direccionForanea, sizeof(direccionForanea));
		direccionForanea.sin_family = AF_INET;
		direccionForanea.sin_addr.s_addr = inet_addr("127.0.0.1");
		direccionForanea.sin_port = htons(puertoVallarta);
		
		/* rellena la direcciòn del cliente*/
		bzero((char *)&direccionLocal, sizeof(direccionLocal));
		direccionLocal.sin_family = AF_INET;
		direccionLocal.sin_addr.s_addr = INADDR_ANY;
		direccionLocal.sin_port = htons(0);
		bind(s, (struct sockaddr *)&direccionLocal,sizeof(direccionLocal));
	}
}

SocketDatagrama::~SocketDatagrama(){

}

//Recibe un paquete tipo datagrama proveniente de este socket
int SocketDatagrama::recibe(PaqueteDatagrama *p, int flag){
	if(flag == 0){
		int clilen = sizeof(direccionForanea);
		int *lel = &clilen;
		return recvfrom(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, (struct sockaddr *)&direccionForanea, (socklen_t*)lel);
	}else{
		return recvfrom(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, NULL, NULL);
	}
}

//Envía un paquete tipo datagrama desde este socket
int SocketDatagrama::envia(PaqueteDatagrama *p, int flag){
	if(flag == 0){
		char* res = (char*)malloc(100*sizeof(char));
		strcpy(res, "Adios");
		p->inicializaDatos(res);
		return sendto(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, (struct sockaddr *)&direccionForanea, sizeof(direccionForanea));
	}else{
		//ESTO ES IMPORTANTE PORQUE AQUI SE REASIGNA LA IP DEL SERVER
		direccionForanea.sin_addr.s_addr = inet_addr(p->obtieneDireccion());
		return sendto(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, (struct sockaddr *)&direccionForanea, sizeof(direccionForanea));
	}

}
char* SocketDatagrama::obtieneIpR(){
	 char *lel = inet_ntoa(direccionForanea.sin_addr);
	 return lel;
}
int SocketDatagrama::obtienePuertoR(){
	 return direccionForanea.sin_port;
}
