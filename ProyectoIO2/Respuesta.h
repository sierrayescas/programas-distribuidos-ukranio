#ifndef Respuesta_h_
#define Respuesta_h_
#include "mensaje.h"
#include "SocketDatagrama.h"

class Respuesta{
	private:
        SocketDatagrama *socketLocal;
		//Mensaje mensaje;
    public:
        Respuesta(int puertoServidor);
        Mensaje getRequest(void);
        //void sendReply(char *datos, char *ipCliente, int PuertoCliente, int requestId);
        //void sendReplyBroadcast(char *datos, char *ipCliente, int PuertoCliente, int requestId);
};
#endif