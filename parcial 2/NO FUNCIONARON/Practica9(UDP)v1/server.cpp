#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

#include <arpa/inet.h>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include <string.h>
using namespace std;

int puerto = 7200;

int main(void){
	unsigned int lon = 1000;
	SocketDatagrama sd(puerto, 0);
	PaqueteDatagrama rec(lon), env(lon);
	while(1){
		sd.recibe(&rec, 0);
		printf("Recive desde desde:  %s\n", sd.obtieneIpR());
		printf("Por el puerto :  %d\n", sd.obtienePuertoR());
      //IMPRIME MENSAJE DEL CLIENTE de LOL
      char* res = (char*)malloc(100*sizeof(char));
      strcpy(res, rec.obtieneDatos());
      cout << "Mensaje del cliente: " << res << endl;
		sd.envia(&env, 0);
	}
	return 0;
}
