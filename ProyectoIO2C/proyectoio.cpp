#include "gfxModified.h"
#include "Coordenada.h"
#include "Tank.h"

#include <iostream>
#include <string.h>
#include <cmath>
#include <unistd.h>
#include <sys/time.h>
#include <sstream>
#include <string>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>

using namespace std;

//Resolución de la pantalla
#define ANCHURA 500
#define ALTURA 500
#define PI 3.1415
double velocidad;

void movPlayer(Tank *t1, bool keys[7]);
void updateDegrees(Tank *t1, bool keys[7]);
void drawPlayer(Tank *t1);
void InicialSet(Tank *tankes);	
int main(){
	gfx_open(ANCHURA, ALTURA, "SHIP IO");
	gfx_color(0,0,0);
	//Tank t1;//tanke de prueba
	//t1.setPosicion(Coordenada(0, 50));
	velocidad = 44.444*ANCHURA/50000;
	int cont = 0;
	int i;
	bool keys[7] = {0,0,0,0,0,0,0};
	bool escapeKeyPressed = false; //escape key terminates game while playing
	char str[7];
	float anguloOruga=0;//posiblemente se requiera incluir en la clase tank
	float anguloCanon=0;//posiblemente se requiera incluir en la clase tank
	//actualizacion, se agrego a la clase tank :v
	Tank tankes[8];
	InicialSet(tankes);
	while(1){


		gfx_clear_color(255, 255, 255);
		
		//t1.setPosicion(Coordenada(cont++, 50));
		if (gfx_event_waiting2()) {
			gfx_clear();
			escapeKeyPressed = gfx_keyPress(keys);
            if (escapeKeyPressed) return 0;
			//movimiento del jugador
				/*    //tanke de prueba 
			movPlayer(&t1,keys,&anguloOruga);
			drawPlayer(&t1); //gabo
				*/
			movPlayer(&tankes[0],keys);//da movimiento a un jugador
			for(i=0;i<8;i++){//actualiza la pocision de los tankes en pantalla
				drawPlayer(&tankes[i]);
			}
			gfx_flush();
			//fin movimiento jugador
			//angulos
			updateDegrees(&tankes[0],keys);
			if(tankes[0].getAnguloCanon() < 0)
				sprintf(str, "%0.2f�",tankes[0].getAnguloCanon() * -1);//ca�on jugador
			else
				sprintf(str, "%0.2f�",tankes[0].getAnguloCanon());//ca�on jugador
			gfx_text(ANCHURA-38, 10, str);

			if(tankes[0].getAnguloOruga() < 0)
				sprintf(str, "%0.2f�",tankes[0].getAnguloOruga() * -1);//oruga jugador
			else
				sprintf(str, "%0.2f�",tankes[0].getAnguloOruga());//oruga jugador
			gfx_text(ANCHURA-38, 23, str);
			//fin angulos
		}


		usleep(10000);
	}
	
}
void movPlayer(Tank *t1,bool keys[7]){
	
	double Vx;
	double Vy;
	float angulo = t1->getAnguloOruga();
	float anguloC = t1->getAnguloCanon();
	float aux = angulo * PI / 180;
	float auxCanon = anguloC * PI / 180; 
	Vx = velocidad * cos( (double) (aux));
	Vy = velocidad * sin( (double) (aux));
	double x1,y1,x2,y2,m;//m = pendiente

	if(keys[0]==1){//a
		//t1->setPosicion(Coordenada(t1->getPosicion().obtenerX()-1,t1->getPosicion().obtenerY()));
		angulo = (angulo + 1);//%360;
		t1->setAnguloOruga(angulo);
	}
	else if(keys[1]==1){//w
		t1->setPosicion(Coordenada(t1->getPosicion().obtenerX()+ Vx,t1->getPosicion().obtenerY()-Vy));
	}
	else if(keys[2]==1){//d
		//t1->setPosicion(Coordenada(t1->getPosicion().obtenerX()+1,t1->getPosicion().obtenerY()));
		angulo = (angulo - 1);//&360;
		t1->setAnguloOruga(angulo);
	}
	else if(keys[3]==1){//s
		t1->setPosicion(Coordenada(t1->getPosicion().obtenerX()-Vx,t1->getPosicion().obtenerY()+Vy));
	}
	else if(keys[6]==1){//u shooooooooooooooooooot
		//cout << auxCanon << endl;
		system("aplay disparo.wav");
		m = tan((double)(auxCanon+aux));
		x1 = t1->getPosicion().obtenerX();
		y1 = t1->getPosicion().obtenerY();
		x2 = 1000*cos(auxCanon);
		y2 = y1 + (x2*m);
		if(x1 -x2 > -0.1 && x1 - x2 < 0.1)
			gfx_line(x1,y1, x2, 360 * sin(auxCanon));
		else
			gfx_line(x1,y1, x2,  y2 );
		usleep(1500);
		gfx_flush();
	}
}

//360 = 2piRad
void updateDegrees(Tank  *t1,bool keys[7]){
	float angulo = t1->getAnguloCanon();
	if(keys[4]==1){//h
		if(angulo == 0 )
			angulo = 360;
		else
			angulo = (angulo - 0.5);//%360;
	}
	else if(keys[5]==1){//k
		if(angulo == 360 )
			angulo = 0;
		else
			angulo = (angulo + 0.5);//%360;
	}
	t1->setAnguloCanon(angulo);
}

void drawPlayer(Tank *t1){
	int num_pts = 5;
	double x1 = t1->getPosicion().obtenerX();
	double y1 = t1->getPosicion().obtenerY();
	int angulo = -t1->getAnguloOruga();
	float angle = (float)angulo;
///*con rotacion de cubo
	XPoint pointsarr[] = {{(short int)(x1+sqrt(40)*cos((angle + 135) *PI/180)), (short int)(y1+sqrt(40)*sin((angle + 135)*PI/180))},
	{(short int)(x1+sqrt(40)*cos((angle - 135) *PI/180)), (short int)(y1+sqrt(40)*sin((angle - 135)*PI/180))},
	{(short int)(x1+sqrt(40)*cos((angle - 45) *PI/180)), (short int)(y1+sqrt(40)*sin((angle - 45)*PI/180))},
	{(short int)(x1+sqrt(40)*cos((angle + 45) *PI/180)), (short int)(y1+sqrt(40)*sin((angle + 45)*PI/180))},
	{(short int)(x1+sqrt(40)*cos((angle + 135) *PI/180)), (short int)(y1+sqrt(40)*sin((angle + 135)*PI/180))}};
//*/
 /*sin rotacion de cubo
	XPoint pointsarr[] = {{t1->getPosicion().obtenerX()-5, t1->getPosicion().obtenerY()-5},
		{t1->getPosicion().obtenerX()-5, t1->getPosicion().obtenerY()+5},
		{t1->getPosicion().obtenerX()+5, t1->getPosicion().obtenerY()+5},
		{t1->getPosicion().obtenerX()+5, t1->getPosicion().obtenerY()-5},
		{t1->getPosicion().obtenerX()-5, t1->getPosicion().obtenerY()-5}
		};

 */
	//DIBUJA EL LA LINEA DE CA�ON
	float aO = t1->getAnguloOruga();
	float aC = t1->getAnguloCanon();
	float anguloDisparo = (aO+aC);
	double x2 = x1 + 10*cos(anguloDisparo*PI/180);
	double y2 = y1 + 10*sin(anguloDisparo*PI/180);
	gfx_line(x1, y1, x2, y2);

	gfx_polygon(pointsarr, num_pts);
}

void InicialSet(Tank *tankes){
	int i;
	for(i=0;i<8;i++){
		if(i<4){
			tankes[i].setPosicion(Coordenada((i+1)*ANCHURA/5,10));
		}
		else{
			tankes[i].setPosicion(Coordenada((i-3)*ANCHURA/5,ALTURA-10));
		}
		//drawPlayer(&tankes[i]);
	}
	
}