#ifndef SOLICITUD_H
#define SOLICITUD_H
#include "SocketDatagrama.h"

class Solicitud{
	public:
		Solicitud(int pto);
		void sendTank(char *IP, int puerto, char *arguments);
		char* receiveTanks();
	private:
		SocketDatagrama *socketlocal;
};
#endif