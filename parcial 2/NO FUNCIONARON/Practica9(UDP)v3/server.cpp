#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

#include <arpa/inet.h>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include <string.h>
using namespace std;

int puerto = 7200;

int main(void){
	unsigned int lon = 65505;
   char* msj = (char*)malloc(100*sizeof(char));

   strcpy(msj, "Amargo adios");

	SocketDatagrama *sd = new SocketDatagrama(puerto);
	PaqueteDatagrama *rec = new PaqueteDatagrama(lon);
   PaqueteDatagrama *env = new PaqueteDatagrama(lon);
	while(1){
		sd->recibe(rec, 0);

      //IMPRIME MENSAJE DEL CLIENTE de LOL
      cout << "Mensaje recibido de: " << rec->obtieneDireccion() << endl;
      cout << "Puerto: " << ntohs(rec->obtienePuerto()) << endl;
      cout << "Mensaje del cliente: " << rec->obtieneDatos() << endl;

      env->inicializaDatos(msj);
		sd->envia(env, 0);
	}
   delete rec;
   delete env;
   delete sd;
   
	return 0;
}
