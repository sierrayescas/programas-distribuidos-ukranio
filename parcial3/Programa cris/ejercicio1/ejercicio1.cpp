#include <iostream>
#include <string.h>
#include <cmath>
#include "gfxModified.h"
#include <unistd.h>
#include <sys/time.h>
#include <sstream>
#include <string>


using namespace std;

//Resolución de la pantalla
#define ANCHURA 950
#define ALTURA 200

int main(){
	struct timeval tv;
	struct tm *tm;

	gfx_open(ANCHURA, ALTURA, "Ejercicio 1: Reloj sincronizado con NTP");
	gfx_color(255,255,255);

	while(1){
		gettimeofday(&tv, NULL);
		tm = localtime(&tv.tv_sec);
		//cout << tm->tm_hour << ":" << tm->tm_min << ":" << tm->tm_sec << ":" << static_cast<int>(tv.tv_usec/10000) << endl;
		gfx_clear();
		/********************** HORAS **********************/
		gfx_display_ascii(10, 20, 10 , (tm->tm_hour/10)+48);
		gfx_display_ascii(100, 20, 10 , (tm->tm_hour%10)+48);
		/*DIbuja los dos puntos*/
		gfx_fill_rectangle(200, 50, 20, 20);
		gfx_fill_rectangle(200, 100, 20, 20);
		/********************** MINUTOS **********************/
		gfx_display_ascii(250, 20, 10 , (tm->tm_min/10)+48);
		gfx_display_ascii(350, 20, 10 , (tm->tm_min%10)+48);
		/*DIbuja los dos puntos*/
		gfx_fill_rectangle(450, 50, 20, 20);
		gfx_fill_rectangle(450, 100, 20, 20);
		/********************** SEGUNDOS **********************/
		gfx_display_ascii(500, 20, 10 , (tm->tm_sec/10)+48);
		gfx_display_ascii(600, 20, 10 , (tm->tm_sec%10)+48);
		/*DIbuja los dos puntos*/
		gfx_fill_rectangle(700, 50, 20, 20);
		gfx_fill_rectangle(700, 100, 20, 20);
		/********************** MILESIMAS DE SEGUNDO **********************/
		gfx_display_ascii(750, 20, 10 , (static_cast<int>(tv.tv_usec/10000)/10)+48);
		gfx_display_ascii(850, 20, 10 , (static_cast<int>(tv.tv_usec/10000)%10)+48);
		gfx_flush();
		usleep(10000);
	}
}
