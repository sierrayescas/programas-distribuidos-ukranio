#include <stdio.h>
#include <math.h>

int main(){
	double seno = 0, coseno = 0, tangente = 0, logaritmo = 0, raizCuad = 0;
	long max = 40000000, i=1;
	while(i < max){
		seno += sin(i);
		coseno += cos(i);
		tangente += tan(i);
		logaritmo += log(i);
		raizCuad += sqrt(i);
		i++;
	}
	return 0;
}