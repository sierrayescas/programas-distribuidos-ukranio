#include <iostream>
#include "Respuesta.h"
#include <string.h>
#include <stdlib.h>
#include <vector>
using namespace std;

int main(int argc, char* argv[]){
	Respuesta resp(atoi(argv[1]));
	struct mensaje *msj= new struct mensaje;
	char *respuesta=(char*)malloc(400);
	int nbd = 0;
	while(1){
		msj = resp.getRequest();
		switch(msj->operationId){
			case 1: //lectura
				cout<<"Se leyó: "<<nbd<<" "<<msj->arguments<<endl;
				sprintf(respuesta, "%d", nbd);
			break;
			case 2: //escritura
				nbd += atoi(msj->arguments);
				cout<<"Escrito: "<<nbd<<endl;
				sprintf(respuesta, "%d", nbd);
			break;
			default:
				cout<<"Operacion no encontrada"<<endl;
		}
		if(nbd != 8){
		resp.sendReply(respuesta,msj->IP,msj->puerto);
		}
	}
	
return 0;
}