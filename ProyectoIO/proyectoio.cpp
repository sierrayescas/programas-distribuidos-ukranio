#include "gfxModified.h"
#include "Coordenada.h"
#include "Tank.h"

#include <iostream>
#include <string.h>
#include <cmath>
#include <unistd.h>
#include <sys/time.h>
#include <sstream>
#include <string>


#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>

using namespace std;

//Resolución de la pantalla
#define ANCHURA 500
#define ALTURA 500
#define PI 3.1415
double velocidad;

void movPlayer(Tank *t1,bool keys[7],float *angulo);
void updateDegrees(float *angulo,bool keys[7]);
int main(){
	gfx_open(ANCHURA, ALTURA, "SHIP IO");
	gfx_color(255,255,255);
	//Tank t1;//tanke de prueba
	Tank tankes[8];
	velocidad = 44.444*ANCHURA/50000;
	int num_pts = 5;
	int cont = 0;
	int i,j;
	
	bool keys[7] = {0,0,0,0,0,0,0};
	 bool escapeKeyPressed = false; //escape key terminates game while playing
	 char str[7];
	 float anguloOruga=0;
	 float anguloCanon=0;
	 //ubicacion inicial tankes
	 //t1.setPosicion(Coordenada(0, 50));
	 for(i=0;i<8;i++){
		if(i<4){
			tankes[i].setPosicion(Coordenada((i+1)*ANCHURA/5,10));
		}
		else{
			tankes[i].setPosicion(Coordenada((i-3)*ANCHURA/5,ALTURA-10));
		}
	}
	while(1){


		gfx_clear_color(25, 25, 25);
		
		//t1.setPosicion(Coordenada(cont++, 50));
		if (gfx_event_waiting2()) {
			gfx_clear();
			escapeKeyPressed = gfx_keyPress(keys);
            if (escapeKeyPressed) return 0;
		//movimiento del jugador
		//tanke de prueba
		/*
		movPlayer(&t1,keys,&anguloOruga);
		XPoint pointsarr[] = {{t1.getPosicion().obtenerX()-5, t1.getPosicion().obtenerY()-5},
		{t1.getPosicion().obtenerX()-5, t1.getPosicion().obtenerY()+5},
		{t1.getPosicion().obtenerX()+5, t1.getPosicion().obtenerY()+5},
		{t1.getPosicion().obtenerX()+5, t1.getPosicion().obtenerY()-5},
		{t1.getPosicion().obtenerX()-5, t1.getPosicion().obtenerY()-5}
		};
		gfx_polygon(pointsarr, num_pts);*/
		//tankes juego
		movPlayer(&tankes[0],keys,&anguloOruga);
		for(i=0;i<8;i++){
		XPoint pointsarr[] = {{tankes[i].getPosicion().obtenerX()-5, tankes[i].getPosicion().obtenerY()-5},
		{tankes[i].getPosicion().obtenerX()-5, tankes[i].getPosicion().obtenerY()+5},
		{tankes[i].getPosicion().obtenerX()+5, tankes[i].getPosicion().obtenerY()+5},
		{tankes[i].getPosicion().obtenerX()+5, tankes[i].getPosicion().obtenerY()-5},
		{tankes[i].getPosicion().obtenerX()-5, tankes[i].getPosicion().obtenerY()-5}
		};
		gfx_polygon(pointsarr, num_pts);
		}
		gfx_flush();
		//fin movimiento jugador
		//angulos
		sprintf(str, "%0.2f�",anguloCanon);//ca�on
		gfx_text(ANCHURA-38, 10, str);
		sprintf(str, "%0.2f�",anguloOruga);//oruga
		gfx_text(ANCHURA-38, 23, str);
		updateDegrees(&anguloCanon,keys);
		//fin angulos
		}
		usleep(10000);
	}
	
}
void movPlayer(Tank *t1,bool keys[7],float *angulo){
	double Vx;
	double Vy;
	float aux = *angulo*PI/180;
	Vx = velocidad*cos((double)(aux));
	Vy = velocidad*sin((double)(aux));
	if(keys[0]==1){//a
		//t1->setPosicion(Coordenada(t1->getPosicion().obtenerX()-1,t1->getPosicion().obtenerY()));
		*angulo= (*angulo +2);//%360;
	}
	else if(keys[1]==1){//w
		t1->setPosicion(Coordenada(t1->getPosicion().obtenerX()+ Vx,t1->getPosicion().obtenerY()-Vy));
	}
	else if(keys[2]==1){//d
		//t1->setPosicion(Coordenada(t1->getPosicion().obtenerX()+1,t1->getPosicion().obtenerY()));
		*angulo=(*angulo-2);//&360;
	}
	else if(keys[3]==1){//s
		t1->setPosicion(Coordenada(t1->getPosicion().obtenerX()-Vx,t1->getPosicion().obtenerY()+Vy));
	}
}

//360 = 2piRad
void updateDegrees(float *angulo,bool keys[7]){
	if(keys[4]==1){//h
		*angulo= (*angulo +1.15);//%360;
	}
	else if(keys[5]==1){//k
		*angulo=(*angulo-1.15);//&360;
	}
}
