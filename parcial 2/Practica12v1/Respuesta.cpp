#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>
#include <arpa/inet.h>
#include "Respuesta.h"
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include <string.h>
#include <stdlib.h>
using namespace std;
Respuesta::Respuesta(int pl){
	socketlocal = new SocketDatagrama(pl);
}
struct mensaje* Respuesta::getRequest(void){
	unsigned int lon = sizeof(struct mensaje);
	PaqueteDatagrama rec(lon);
	struct mensaje *msj= new struct mensaje;
	socketlocal->recibe(&rec);
	memcpy(msj,rec.obtieneDatos(),sizeof(struct mensaje));
	msj->puerto=rec.obtienePuerto();
	memcpy(msj->IP,rec.obtieneDireccion(),16);
	return msj;
}
void Respuesta::sendReply(char *respuesta, char *ipCliente, int puertoCliente){
	unsigned int lon = sizeof(struct mensaje);
	PaqueteDatagrama env(lon);
	struct mensaje men;
	memcpy(&men.arguments,respuesta,400);
	men.messageType=1;
	men.requestId=1;
	memcpy(&men.IP,ipCliente,16);
	men.puerto= puertoCliente;
	men.operationId=0;
	char msj[sizeof(struct mensaje)];
	memcpy(msj,&men,sizeof(struct mensaje));  
	env.inicializaPuerto(men.puerto);
    env.inicializaIp(men.IP);
	env.inicializaDatos(msj);
	socketlocal->envia(&env);
}