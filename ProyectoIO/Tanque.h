#include "Coordenada.h"
#include <iostream>
#include <math.h>
using namespace std;

Tanque::Tanque(double xx, double yy){
	x = xx;
	y = yy;
}

double Coordenada::obtenerX(){
	return x;
}

double Coordenada::obtenerY(){
	return y;
}

void setPosicion(Coordenada ps){
	posicion = ps;
}

Coordenada getPosicion(){
	return posicion;
}