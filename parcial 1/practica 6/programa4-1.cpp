#include <iostream>
#include <vector>
#include <math.h>
#include <algorithm>    // std::sort
using namespace std;

class Coordenada{
	private:
		double x;
		double y;
		double magnitud;
 	public:
		Coordenada(double = 0, double = 0);
		double obtenerX();
		double obtenerY();
		double obtenerM();
};

class PoligonoIrreg{
 	private:
		vector<Coordenada> vec;
 	public:
		PoligonoIrreg();
		void anadeVertice(Coordenada);
		void imprimeVertices();
		void ordenaA();
};

bool myfunction(Coordenada i, Coordenada j){ 
	return (i.obtenerM() < j.obtenerM());
}

int main(){
	PoligonoIrreg pi;
	int i;
	for(i = 0; i < 10; i++){
		pi.anadeVertice(Coordenada(drand48()*(100-(-100)+1)-100, drand48()*(100-(-100)+1)-100));
	}
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(3);
	pi.imprimeVertices();
	pi.ordenaA();
	

	return 0;
}

PoligonoIrreg::PoligonoIrreg(){
	;
}
void PoligonoIrreg::anadeVertice(Coordenada coo){
	vec.push_back(coo);
}
void PoligonoIrreg::imprimeVertices(){
	vector<Coordenada>::iterator i; //ITERADOR DE COORDENADAS
	cout << "Los vertices son: ";
	for(i = vec.begin(); i != vec.end(); i++){
		cout << " (" << (*i).obtenerX() << ", " << (*i).obtenerY() << ")" << " Magnitud: " << (*i).obtenerM() << endl;
	}
	cout << endl;
}
void PoligonoIrreg::ordenaA(){
	sort(vec.begin(), vec.end(), myfunction);
	vector<Coordenada>::iterator it; //ITERADOR PODEROSO OP
  	for(it = vec.begin(); it != vec.end(); it++){
		cout << " (" << (*it).obtenerX() << ", " << (*it).obtenerY() << ")" << " Magnitud: " << (*it).obtenerM() << endl;
	}
}


Coordenada::Coordenada(double xx, double yy) : x(xx), y(yy){
	magnitud = sqrt(pow(xx, 2) + pow(yy, 2));
}
double Coordenada::obtenerX(){
	return x;
}
double Coordenada::obtenerY(){
	return y;
}
double Coordenada::obtenerM(){
	return magnitud;
}