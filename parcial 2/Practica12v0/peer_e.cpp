#include <iostream>
#include "Solicitud.h"
#include <string.h>
#include <stdlib.h>
using namespace std;

int main(int argc, char *argv[]){
	Solicitud cliente;//ip, puerto,operationId, n
	char resp[4000];
	char envia[4000];
	int i, v, n, temp;
	int identificador=0;
	v = atoi(argv[3]); 
	for(i = 0; i < v; i++){
		strcpy(envia,"lec");
		memcpy(resp,cliente.doOperation(argv[1],atoi(argv[2]),1,envia,identificador),4000); //lee
		identificador++;
		if(!strcmp(resp,"Operacion realizada anteriormente")){
			cout<<"Operacion con ID de mensaje: "<<identificador-1<<" y ID de operacion: 1 se recibio repetida, si no recibio los datos, favor de reingresar la operacion\n No es posible efectar operacion deposito"<<endl;
			i--;
		}
		else{
		if(!strcmp(resp, "Servidor no disponible, intenta mas tarde baka")){
			break;
		}else{
			n = atoi(resp);
			if(n == i){
				printf("Leyó %d y si es: %d\n", n, i);
				n++;
				temp = sprintf(envia, "%d", n);
				memcpy(resp,cliente.doOperation(argv[1],atoi(argv[2]),2,envia,identificador),4000); //escribe
				if(!strcmp(resp,"Operacion realizada anteriormente")){
					cout<<"Operacion con ID de mensaje: "<<identificador<<" y ID de operacion: 2 se recibio repetida, pero solo se ejecuto 1 vez"<<endl;
				}
				identificador++;
				if(!strcmp(resp, "Servidor no disponible, intenta mas tarde baka")){
					break;
				}else{
					if(atoi(resp) != n){
						printf("El numero escrito y el devuelto por el server son diferentes.\n");
					}
				}
			}else{
				printf("Leyó %d y deberia ser: %d\n", n, i);
				break;
			}
		}
	}
	}
	
	cout<<resp<<endl;
	return 0;
}
