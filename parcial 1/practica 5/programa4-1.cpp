#include <iostream>
#include <vector>
using namespace std;

class Coordenada{
	private:
		double x;
		double y;
 	public:
		Coordenada(double = 0, double = 0);
		double obtenerX();
		double obtenerY();
};

class Rectangulo{
 	private:
		Coordenada superiorIzq;
		Coordenada inferiorDer;
 	public:
		Rectangulo();
		Rectangulo(double xSupIzq, double ySupIzq, double xInfDer, double yInfDer);
		void imprimeEsq();
		Coordenada obtieneSupIzq();
		Coordenada obtieneInfDer();
};

class PoligonoIrreg{
 	private:
		vector<Coordenada> vec;
 	public:
		PoligonoIrreg();
		void anadeVertice(Coordenada);
		void imprimeVertices();
};

int main(){
	PoligonoIrreg pi;
	pi.anadeVertice(Coordenada(1,2));
	pi.anadeVertice(Coordenada(3,3));
	pi.anadeVertice(Coordenada(4,7));
	pi.anadeVertice(Coordenada(5,8));
	pi.imprimeVertices();
	return 0;
}

PoligonoIrreg::PoligonoIrreg(){
	;
}

void PoligonoIrreg::anadeVertice(Coordenada coo){
	vec.push_back(coo);
}
void PoligonoIrreg::imprimeVertices(){
	cout << "Los vertices son: ";
	int i;
	for(i = 0; i < vec.size(); i++){
		cout << " (" << vec[i].obtenerX() << "," << vec[i].obtenerY() << ")";
	}
	cout << endl;
}



Coordenada::Coordenada(double xx, double yy) : x(xx), y(yy){
}
double Coordenada::obtenerX(){
	return x;
}
double Coordenada::obtenerY(){
	return y;
}


Rectangulo::Rectangulo() : superiorIzq(0,0), inferiorDer(0,0){
}
Rectangulo::Rectangulo(double xSupIzq, double ySupIzq, double xInfDer, double yInfDer):superiorIzq(xSupIzq, ySupIzq), inferiorDer(xInfDer, yInfDer){
}
void Rectangulo::imprimeEsq(){
	cout << "Para la esquina superior izquierda.\n";
	cout << "x = " << superiorIzq.obtenerX() << " y = " << superiorIzq.obtenerY() << endl;
	cout << "Para la esquina inferior derecha.\n";
	cout << "x = " << inferiorDer.obtenerX() << " y = " << inferiorDer.obtenerY() << endl;
}
Coordenada Rectangulo::obtieneSupIzq(){
	return superiorIzq;
}
Coordenada Rectangulo::obtieneInfDer(){
	return inferiorDer;
}