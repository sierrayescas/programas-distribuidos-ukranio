#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

#include <arpa/inet.h>
#include <stdlib.h>

int puerto = 7200;

int main(int argc, char *argv[])
{
   //PIDE COMO ARGUMENTO LA IP DEL SERVER DE MAINCRA (gabo)
   char* ip_op;
   ip_op = argv[1];
   printf("IP DEL SERVIDOR %s\n", ip_op);

   struct sockaddr_in msg_to_server_addr, client_addr;
   int s, num[16376], res;
   
   s = socket(AF_INET, SOCK_DGRAM, 0);
   /* rellena la dirección del servidor */
   bzero((char *)&msg_to_server_addr, sizeof(msg_to_server_addr));
   msg_to_server_addr.sin_family = AF_INET;
   msg_to_server_addr.sin_addr.s_addr = inet_addr(ip_op);
   msg_to_server_addr.sin_port = htons(puerto);
   
   /* rellena la direcciòn del cliente*/
   bzero((char *)&client_addr, sizeof(client_addr));
   client_addr.sin_family = AF_INET;
   client_addr.sin_addr.s_addr = INADDR_ANY;
   
   /*cuando se utiliza por numero de puerto el 0, el sistema se encarga de asignarle uno */
   client_addr.sin_port = 7777;
   bind(s, (struct sockaddr *)&client_addr,sizeof(client_addr));
   num[0] = 2;
   num[1] = 5; /*rellena el mensaje */
   sendto(s, (char *)num, 16376 * sizeof(int), 0, (struct sockaddr *) &msg_to_server_addr, sizeof(msg_to_server_addr));
   
   /* se bloquea esperando respuesta */
   recvfrom(s, (char *)&res, sizeof(int), 0, NULL, NULL);
   //printf("%s\n", inet_ntoa(msg_to_server_addr));
   printf("Puerto: %d\n", msg_to_server_addr.sin_port);
   char *lel = inet_ntoa(msg_to_server_addr.sin_addr);
   printf("Dirección: %s\n", lel);
   printf("2 + 5 = %d\n", res);
   close(s);
}

