#include <iostream>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include <sys/time.h>
#include <errno.h>
using namespace std;

SocketDatagrama::SocketDatagrama(int puertoVallarta){
	s = socket(AF_INET, SOCK_DGRAM, 0);

	bzero((char *)&direccionLocal, sizeof(direccionLocal));
	bzero((char *)&direccionForanea, sizeof(direccionForanea));
	direccionLocal.sin_family = AF_INET;
	direccionForanea.sin_family = AF_INET;
	direccionLocal.sin_addr.s_addr = INADDR_ANY;
	direccionLocal.sin_port = htons(puertoVallarta);
	bind(s, (struct sockaddr *)&direccionLocal, sizeof(direccionLocal));
}

SocketDatagrama::~SocketDatagrama(){
	close(s);
}

//Recibe un paquete tipo datagrama proveniente de este socket
int SocketDatagrama::recibe(PaqueteDatagrama *p){
	int clilen = sizeof(direccionForanea);
	int *lel = &clilen;
	int temp = recvfrom(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, (struct sockaddr *)&direccionForanea, (socklen_t*)lel);
	p->inicializaIp(inet_ntoa(direccionForanea.sin_addr));
	p->inicializaPuerto(ntohs(direccionForanea.sin_port));
	return temp;
}

//Envía un paquete tipo datagrama desde este socket
int SocketDatagrama::envia(PaqueteDatagrama *p){
	direccionForanea.sin_addr.s_addr = inet_addr(p->obtieneDireccion());
	direccionForanea.sin_port = htons(p->obtienePuerto());
	return sendto(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, (struct sockaddr *)&direccionForanea, sizeof(direccionForanea));
}

int SocketDatagrama::recibeTimeout(PaqueteDatagrama *p, time_t segundos, suseconds_t microsegundos){
	struct timeval timeout;
	timeout.tv_sec = segundos;
	timeout.tv_usec = microsegundos;
	setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
	int clilen = sizeof(direccionForanea);
	int *lel = &clilen;
	int temp = recvfrom(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, (struct sockaddr *)&direccionForanea, (socklen_t*)lel);
	
	p->inicializaIp(inet_ntoa(direccionForanea.sin_addr));
	p->inicializaPuerto(ntohs(direccionForanea.sin_port));
	if(temp < 0){
		if(errno == EWOULDBLOCK){
			fprintf(stderr, "Tiempo para recepción transcurrido\n");
			temp = -1;
		}else{
			fprintf(stderr, "Error en recvfrom\n");
		}
	}
	return temp;
}