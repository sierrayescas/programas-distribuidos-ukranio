#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

#include <arpa/inet.h>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include <string.h>
using namespace std;

int puerto = 7200;

int main(int argc, char *argv[]){
	unsigned int lon = 1000;
	char* ip_op = argv[1];
	char* msj = (char*)malloc(100*sizeof(char));
	char* res = (char*)malloc(100*sizeof(char));
	strcpy(msj, "Hola");
	SocketDatagrama sd(puerto, 1);
	printf("LOL\n");
	PaqueteDatagrama rec(lon), env(lon);
	printf("HEHE\n");
	env.inicializaDatos(msj);
	printf("HAHA\n");
	env.inicializaIp(ip_op);
	printf("Envia desde:  %s\n", env.obtieneDireccion());
	env.inicializaPuerto(puerto);
	printf("LEL\n");
	sd.envia(&env, 1);
	printf("LAL\n");
	sd.recibe(&rec, 1);
	printf("Recive desde desde:  %s\n", sd.obtieneIpR());
	printf("Por el puerto :  %d\n", sd.obtienePuertoR());
	strcpy(res, rec.obtieneDatos());
	cout << "Respuesta: " << res << endl;
	return 0;
}
