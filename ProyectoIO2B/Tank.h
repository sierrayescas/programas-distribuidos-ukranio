#ifndef TANK_H_
#define TANK_H_
#include "Coordenada.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
class Tank{
 	private:
		int rojo;
		int azul;
		int verde;
		Coordenada posicion;
		float angulooruga;
		float anguloCanon;
		bool isAlive;
 	public:
		Tank();
		void setPosicion(Coordenada ps);
		Coordenada getPosicion();
		void setAnguloOruga(float org);
		float getAnguloOruga();
		void setAnguloCanon(float org);
		float getAnguloCanon();
};
#endif