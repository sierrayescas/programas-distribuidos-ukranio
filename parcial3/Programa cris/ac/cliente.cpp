#include "sockets/SocketDatagrama.h"  
#include "sockets/PaqueteDatagrama.h"
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <sys/time.h>
#include "gfxModified.h"

using namespace std;

#define ANCHURA 950
#define ALTURA 200

int main() {
  int donnee_envoyer = 1;
  unsigned long long int total_temps_difference,total_temps_recu, aux;
  struct timeval temps_debut, temps_fin ,temps_difference;
  struct timeval *temps_recu;
  struct timespec tiempox,tiempoy;
  struct timeval temps_nouveau;
  struct tm *tm;
  gfx_open(ANCHURA, ALTURA, "Ejercicio 3: Reloj sincronizado Con algoritmo de cristian");
	gfx_color(255,255,255);
	SocketDatagrama sock_client = SocketDatagrama(7000);
  	PaqueteDatagrama paquet_envoyer = PaqueteDatagrama(&donnee_envoyer,sizeof(int),"10.100.76.245",7777);
  	PaqueteDatagrama paquet_recu = PaqueteDatagrama(sizeof(struct timeval));
  	sock_client.envia(paquet_envoyer);
  gettimeofday(&temps_debut,NULL);
  clock_gettime(CLOCK_REALTIME,&tiempox);
  sock_client.recibeTimeout(paquet_recu);
  gettimeofday(&temps_fin,NULL);
  clock_gettime(CLOCK_REALTIME,&tiempoy);
  temps_recu = (struct timeval *) paquet_recu.obtieneDatos();
  ///timersub(&temps_fin,&temps_debut,&temps_difference);
  total_temps_difference = (abs(tiempox.tv_sec - tiempoy.tv_sec)*1e6) +(abs(tiempox.tv_nsec - tiempoy.tv_nsec)/1e3);
  total_temps_recu = (temps_recu->tv_sec*1e6) + (temps_recu->tv_usec);
  cout << "total_temps_difference: " << total_temps_difference <<endl;
  cout << "total_temps_recu: " << total_temps_recu <<endl;
  //total_temps_difference = (temps_difference.tv_sec*1e6) 
    //+ (temps_difference.tv_usec);
  aux = ((total_temps_recu) + (total_temps_difference*.5));
  temps_nouveau.tv_sec = (time_t) aux/1e6;
  temps_nouveau.tv_usec = (suseconds_t)aux%1000000;

  //cout << "le temps recu est secondes -> " << temps_recu->tv_sec 
    //<<"usecondes -> "<<temps_recu->tv_usec << endl; 
  cout << "Tiempo recibido es segundos -> " << temps_recu->tv_sec 
    <<"usegundos -> "<<temps_recu->tv_usec << endl; 

  //cout << "le temps de la different est secondes -> " 
   // << temps_difference.tv_sec <<"usecondes -> "
    //<<temps_difference.tv_usec << endl; 
  cout << "Tiempo de diferencia es segundos -> " 
    << total_temps_difference/1e6 <<"usegundos -> "
    << total_temps_difference%1000000 << endl; 

  //cout << "le temps nouveau est secondes -> " << temps_nouveau.tv_sec 
    //<< "usecondes -> "  <<  temps_nouveau.tv_usec << endl; 
  cout << "EL tiempo nuevo es segundos  -> " << temps_nouveau.tv_sec 
    << "usegundos -> "  <<  temps_nouveau.tv_usec << endl; 

    //temps_nouveau.tv_sec = 1557161556;
    //temps_nouveau.tv_usec = 0;

settimeofday(&temps_nouveau,NULL);
  while(1)
  {  
  	gettimeofday(&temps_nouveau,NULL);
  	tm = localtime(&temps_nouveau.tv_sec);

  	gfx_clear();
	/********************** HORAS **********************/
	gfx_display_ascii(10, 20, 10 , (tm->tm_hour/10)+48);
	gfx_display_ascii(100, 20, 10 , (tm->tm_hour%10)+48);
	/*DIbuja los dos puntos*/
	gfx_fill_rectangle(200, 50, 20, 20);
	gfx_fill_rectangle(200, 100, 20, 20);
	/********************** MINUTOS **********************/
	gfx_display_ascii(250, 20, 10 , (tm->tm_min/10)+48);
	gfx_display_ascii(350, 20, 10 , (tm->tm_min%10)+48);
	/*DIbuja los dos puntos*/
	gfx_fill_rectangle(450, 50, 20, 20);
	gfx_fill_rectangle(450, 100, 20, 20);
	/********************** SEGUNDOS **********************/
	gfx_display_ascii(500, 20, 10 , (tm->tm_sec/10)+48);
	gfx_display_ascii(600, 20, 10 , (tm->tm_sec%10)+48);
	/*DIbuja los dos puntos*/
	gfx_fill_rectangle(700, 50, 20, 20);
	gfx_fill_rectangle(700, 100, 20, 20);
	/********************** MILESIMAS DE SEGUNDO **********************/
	gfx_display_ascii(750, 20, 10 , (static_cast<int>(temps_nouveau.tv_usec/10000)/10)+48);
	gfx_display_ascii(850, 20, 10 , (static_cast<int>(temps_nouveau.tv_usec/10000)%10)+48);
	gfx_flush();
	usleep(10000);

  }
  //system("timedatectl");
return 0; 
}

