#include <iostream>
#include <string.h>

#include <arpa/inet.h>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
using namespace std;

SocketDatagrama::SocketDatagrama(int puertoVallarta, int flag){
	s = socket(AF_INET, SOCK_DGRAM, 0);

	if(flag == 0){
		/* rellena la dirección del servidor */
		bzero((char *)&direccionLocal, sizeof(direccionLocal));
		direccionLocal.sin_family = AF_INET;
		direccionLocal.sin_addr.s_addr = INADDR_ANY;
		direccionLocal.sin_port = htons(7200);
		bind(s, (struct sockaddr *)&direccionLocal, sizeof(direccionLocal));
	}else{
		/* rellena la dirección del servidor */
		bzero((char *)&direccionForanea, sizeof(direccionForanea));
		direccionForanea.sin_family = AF_INET;
		direccionForanea.sin_addr.s_addr = inet_addr("127.0.0.1");
		direccionForanea.sin_port = htons(7200);
		
		/* rellena la direcciòn del cliente*/
		bzero((char *)&direccionLocal, sizeof(direccionLocal));
		direccionLocal.sin_family = AF_INET;
		direccionLocal.sin_addr.s_addr = INADDR_ANY;
		if(puertoVallarta == 0){
			printf("WTF\n");
			direccionLocal.sin_port = htons(0);
		}else{
			direccionLocal.sin_port = htons(puertoVallarta);
		}
		bind(s, (struct sockaddr *)&direccionLocal,sizeof(direccionLocal));
	}
}

SocketDatagrama::~SocketDatagrama(){

}

//Recibe un paquete tipo datagrama proveniente de este socket
int SocketDatagrama::recibe(PaqueteDatagrama *p, int flag){
	if(flag == 0){
		int clilen = sizeof(direccionForanea);
		int *lel = &clilen;
		int temp = recvfrom(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, (struct sockaddr *)&direccionForanea, (socklen_t*)lel);
		p->inicializaIp(inet_ntoa(direccionForanea.sin_addr));
		p->inicializaPuerto(direccionForanea.sin_port);
		return temp;

	}else{
		int temp = recvfrom(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, NULL, NULL);
		p->inicializaPuerto(direccionForanea.sin_port);
		return temp;	
	}
}

//Envía un paquete tipo datagrama desde este socket
int SocketDatagrama::envia(PaqueteDatagrama *p, int flag){
	if(flag == 0){
		return sendto(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, (struct sockaddr *)&direccionForanea, sizeof(direccionForanea));
	}else{
		//ESTO ES IMPORTANTE PORQUE AQUI SE REASIGNA LA IP DEL SERVER
		direccionForanea.sin_addr.s_addr = inet_addr(p->obtieneDireccion());
		return sendto(s, (char *) p->obtieneDatos(), p->obtieneLongitud()*sizeof(char), 0, (struct sockaddr *)&direccionForanea, sizeof(direccionForanea));
	}

}
