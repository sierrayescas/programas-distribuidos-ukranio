#include "Coordenada.h"
#include "Tank.h"
#include <iostream>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
using namespace std;

Tank::Tank(){
	setAnguloCanon(0);
	setAnguloOruga(0);
}

void Tank::setPosicion(Coordenada ps){
	posicion = ps;
}

Coordenada Tank::getPosicion(){
	return posicion;
}

void Tank::setAnguloOruga(float ang){
	angulooruga = ang;
}

float Tank::getAnguloOruga(){
	return angulooruga;
}

void Tank::setAnguloCanon(float ang){
	anguloCanon = ang;
}

float Tank::getAnguloCanon(){
	return anguloCanon;
}
