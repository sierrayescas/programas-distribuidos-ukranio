#include "SocketDatagrama.h" 
#include "PaqueteDatagrama.h"
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <math.h>
#include <string>


using namespace std;

struct timeval tiempo2;
struct timespec tiempo1,tiempo3;
bool axu2 = true;


SocketDatagrama::SocketDatagrama(int pto) {
	s = socket(AF_INET, SOCK_DGRAM,0);
	bzero((char *)&direccionLocal,sizeof(direccionLocal));
	direccionLocal.sin_family = AF_INET;
	direccionLocal.sin_addr.s_addr=INADDR_ANY;
	direccionLocal.sin_port = htons(pto);
	bind(s,(struct sockaddr *)&direccionLocal,
	sizeof(direccionLocal));
}

SocketDatagrama::SocketDatagrama() {
	s = socket(AF_INET, SOCK_DGRAM,0);
	bzero((char *)&direccionLocal,sizeof(direccionLocal));
	direccionLocal.sin_family = AF_INET;
	direccionLocal.sin_addr.s_addr=INADDR_ANY;
	direccionLocal.sin_port = htons(0);
	bind(s,(struct sockaddr *)&direccionLocal,
	sizeof(direccionLocal));
}


int SocketDatagrama::recibe(PaqueteDatagrama &p) {
	socklen_t lon = sizeof(direccionForanea);
   recvfrom(s, p.obtieneDatos(), p.obtieneLongitud(), 0,
  (struct sockaddr *)&direccionForanea, &lon);
	cout << inet_ntoa(direccionForanea.sin_addr) << endl;
	p.inicializaIP(inet_ntoa(direccionForanea.sin_addr));
	p.inicializaPuerto(htons(direccionForanea.sin_port));
	string aux_string(
        inet_ntoa(direccionForanea.sin_addr));	
	arrdirip.push_back(aux_string);
	arrd2.push_back(aux_string);
	//cout << aux_string << endl;
        clock_gettime(CLOCK_REALTIME,&tiempoy);
	llego = axu2;
	axu2 = true;
long int lapso,neufsegundos=0;
long double aux1=0,neufmicro=0;
        lapso = labs((long int)((tiempoy.tv_sec-tiempox.tv_sec)));
	aux1 += (long double) lapso * 1e6; 
        lapso = labs((long int)
		((tiempoy.tv_nsec-tiempox.tv_nsec)/1000));
	aux1 += (long double) lapso;	
	delta =  aux1 -srtt;
	srtt = srtt + 0.125 * delta;
	rttvar = rttvar  + 0.25 * 
		(((long double)labs(delta)) - rttvar);
	aux1 = srtt + 4*rttvar;
	if(aux1 > 1e6) {
		neufsegundos = (long int)ceill(aux1/1e6);
		neufmicro = (((long double)neufsegundos)-(aux1/1e6))*1e6;
		//tiempo_time_out.it_value.tv_sec = neufsegundos;
		//tiempo_time_out.it_value.tv_usec = neufmicro;
	}else {
		;
		//tiempo_time_out.it_value.tv_sec = 0;	
		//tiempo_time_out.it_value.tv_usec = aux1;
	}
	aux1 =0 ;
	delta = 0;
	

return 0;
}

//int SocketDatagrama::envia2(PaqueteDatagrama &p) {}

int SocketDatagrama::envia(PaqueteDatagrama &p) {
bzero((char *)&direccionForanea, sizeof(direccionForanea));
   direccionForanea.sin_family = AF_INET;
   direccionForanea.sin_port = htons(p.obtienePuerto());
   direccionForanea.sin_addr.s_addr = inet_addr(p.obtieneDireccion());
	llego = false;
clock_gettime(CLOCK_REALTIME,&tiempox);
	sendto(s,(char *)p.obtieneDatos(),p.obtieneLongitud(),0,
	(struct sockaddr *)&direccionForanea,sizeof(direccionForanea));
return 0;
}

void SocketDatagrama::setTimeout
     (time_t segundos, useconds_t microsegundos) {
  	if( segundos <=0 || microsegundos < 0) {
		cout << "Valores erroneos" <<endl;
	} else {
		tiempo_time_out.it_interval.tv_sec = 0;
		tiempo_time_out.it_interval.tv_usec = 0;
		tiempo_time_out.it_value.tv_sec = segundos;	
		tiempo_time_out.it_value.tv_usec = microsegundos*1e3;	
		tiempo2.tv_sec =tiempo_time_out.it_value.tv_sec ; 
		tiempo2.tv_usec =tiempo_time_out.it_value.tv_usec ; 
		srtt = 0.0;
		rttvar = 3.0*1e6;
	}
	timeout = true;
	
}

void SocketDatagrama::unsetTimeout() { timeout = false; }

void tratar_alarma(int id) {
axu2 = false;
//cout << "Tiempo para recepcion transcurrido" << endl;
//printf("Tiempo para recepcion transcurrido Tiempo %lds %ldms\n",
//tiempo2.tv_sec,tiempo2.tv_usec);
//exit(-1);
}

void SocketDatagrama::recibeTimeout(PaqueteDatagrama &p) {
	act.sa_handler = tratar_alarma;
	act.sa_flags=0;
	sigemptyset(&mask);
	sigaddset(&mask,SIGQUIT);
	sigprocmask(SIG_SETMASK,&mask,NULL);
	sigaction(SIGALRM,&act,NULL);
	if(timeout)
	setitimer(ITIMER_REAL,&tiempo_time_out,NULL);
	recibe(p);
}

void SocketDatagrama::setBroadcast() {
	int broadcastEnable = 1;
	setsockopt(s,SOL_SOCKET,SO_BROADCAST,
	&broadcastEnable,sizeof(broadcastEnable));
	
}

