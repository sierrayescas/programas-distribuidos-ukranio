#include "Solicitud.h"
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include "mensaje.h"
#include <string.h>
#include <iostream>
using namespace std;
Solicitud::Solicitud(){
	socketlocal = new SocketDatagrama(0);
}
char* Solicitud::doOperation(char *IP, int puerto, int operationId, char *arguments){
	unsigned int lon = sizeof(struct mensaje);
	PaqueteDatagrama env(lon);
	PaqueteDatagrama rec(lon);
	struct mensaje men;
	char msj[sizeof(struct mensaje)];
	env.inicializaPuerto(puerto);
    env.inicializaIp(IP);
	memcpy(&men.arguments,arguments,4000);
	//cout<<"argumento en men: "<<men.arguments<<endl;
	//cout<<"argumento recivido: "<<arguments<<endl;
	//cout<<"sizeof men.arg"<<sizeof(men.arguments)<<endl;
	//cout<<"sizeof argumentos"<<sizeof(arguments)<<endl;
	men.messageType=0;
	men.requestId=0;
	memcpy(&men.IP,env.obtieneDireccion(),16);
	men.puerto= env.obtienePuerto();
	men.operationId=0;
	memcpy(msj,&men,sizeof(struct mensaje));
	env.inicializaDatos(msj);
	socketlocal->envia(&env);
	socketlocal->recibe(&rec);
	memcpy(&men,rec.obtieneDatos(),sizeof(struct mensaje));
	//cout<<men.arguments<<"Si recive respuesta"<<endl;
	char *respuesta = men.arguments;
	return respuesta;
}