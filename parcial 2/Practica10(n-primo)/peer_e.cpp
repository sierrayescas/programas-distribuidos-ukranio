#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

#include <arpa/inet.h>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include <string.h>
#include <stdlib.h>
using namespace std;

int main(int argc, char *argv[]){
	unsigned int lon = 500;

	int puerto = atoi(argv[1]); //PUERTO LOCAL
    char* ip_op = argv[2]; //IP DEL SERVER PRINCIPAL

	SocketDatagrama sd(puerto);
	PaqueteDatagrama env(lon);
	PaqueteDatagrama rec(lon);

	env.inicializaPuerto(7200);
    env.inicializaIp(ip_op);
    char msj[lon];
    strcpy(msj, "C");
    strcat(msj, argv[3]);  
    env.inicializaDatos(msj);
	sd.envia(&env);
	sd.recibe(&rec);
	printf("Mensaje del servidor principal.\n%s\n", rec.obtieneDatos());
	return 0;
}
