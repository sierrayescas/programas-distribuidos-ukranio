#include <thread>
#include <iostream>
#include <unistd.h>
#include <string.h>
#include "Respuesta.h"
#include "mensaje.h"
#define puertoServidor 7200
#define puertoPosicion 7201
#define puertoDisparo 7202
using namespace std;

Respuesta respuesta(puertoServidor);
char ipBroadcast[16];
int posiciones[120][2];
int contador=0;
mensaje *recibido;

void Enviar(){
	while(1){
		sleep(1);
		if(contador!=0){
			for(int i=contador;i<120;i++){
				posiciones[i][0]=0;
				posiciones[i][1]=0;
			}
			respuesta.sendReplyBroadcast((char*)posiciones,ipBroadcast,puertoPosicion,1);
			cout<<"Envie las posiciones a "<<ipBroadcast<<" "<<puertoPosicion<<endl;
			contador=0;
		}
	}
}

/*
operationId==1: Recibe posicion
operationId==2: Recibe disparo
*/
void Recibir(){
	struct timeval tiempo;
	int dato[2];
	while(1){		
		recibido=respuesta.getRequest();
		if(recibido->operationId==1){
			memcpy(dato,recibido->arguments,2*sizeof(int));
			posiciones[contador][0]=dato[0];
			posiciones[contador][1]=dato[1];
			contador=contador+1;
			cout<<"La posicion recibida de "<<recibido->IP<<" es "<<dato[0]<<","<<dato[1]<<" "<<contador<<endl;
		}
		else if(recibido->operationId==2){
			respuesta.sendReply((char*)&"Recibi disparo",recibido->IP,puertoDisparo,recibido->requestId);
		}
	}
}

int main(int argc,char* argv[]){
	if(argc!=2){
		cout<<"Ingrese "<<argv[0]<<" ipBroadcast"<<endl;
		exit(-1);
	}
	strcpy(ipBroadcast,argv[1]);
	thread th1(Enviar),th2(Recibir);
	//th1.join();
	th2.join();
}