#include <iostream>
#include "Respuesta.h"
#include <string.h>
#include <stdlib.h>
#include <vector>
using namespace std;
int buscar(int *save,int identificador);
int main(int argc, char* argv[]){
	int save[10]={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
	Respuesta resp(atoi(argv[1]));
	struct mensaje *msj= new struct mensaje;
	char *respuesta=(char*)malloc(4000);
	int nbd = 0, n;
	int x,ss=-1;
	for(;;){
		ss++;
		msj= resp.getRequest();
		if(buscar(save,msj->requestId))
			{
			cout<<"Identificador de mensaje: "<<msj->requestId<<endl;
			save[ss%10]=msj->requestId;
			switch(msj->operationId){
				case 1: //lectura
					cout<<"mensaje cliente: "<<msj->arguments<<endl;
					n = sprintf(respuesta, "%d", nbd);
				break;
				case 2: //escritura
					cout<<"mensaje cliente: "<<msj->arguments<<endl;
					nbd = atoi(msj->arguments);
					n = sprintf(respuesta, "%d", nbd);
	
				break;
				default:
					cout<<"Operacion no encontrada"<<endl;
			}
			cin>>x;
			if(x==1)
				resp.sendReply(respuesta,msj->IP,msj->puerto);
		}
		else{
			cout<<"Operacion con ID de mensaje: " <<msj->requestId<<" ya realizada"<<endl;
			sprintf(respuesta, "Operacion realizada anteriormente");
			resp.sendReply(respuesta,msj->IP,msj->puerto);
		}
	}
	
return 0;
}

int buscar(int *save,int identificador){//devuelve 1 si no lo encontro, 0 si lo encontro
	int i;
	for(i=0;i<10;i++){
		if(save[i]==identificador)
			return 0;
	}
	return 1;
}