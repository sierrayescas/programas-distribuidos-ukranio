#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

#include <arpa/inet.h>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>
using namespace std;

typedef struct{
    char ip[16];
    int puerto;
}Servidor;

void imprime_servidores();
int servidores_length();
int esPrimo(unsigned long int, unsigned long int, unsigned long int);

Servidor arre_ser[5];

int main(int argc, char* argv[]){
	unsigned long int lon = 500;
	
    int tipo = atoi(argv[1]); //TIPO DE SERVIDOR
    int puerto = atoi(argv[2]); //PUERTO LOCAL
    char* ip_op = argv[3]; //IP DEL SERVER PRINCIPAL
	char *prt;

    int i;
    if(tipo == 0){ //EL TIPO 0 ES EL SERVIDOR PRINCIPAL
        SocketDatagrama sd(puerto);
        int i_ser = 0;
        int acumulador = 0; //el que cuenta los resultados de los servidores secundarios
        int cuenta = 0;
        Servidor cliente;

        while(1){
            PaqueteDatagrama rec(lon);
            sd.recibe(&rec);
            char msj[lon];
            strcpy(msj, rec.obtieneDatos());

            if(!strcmp(msj, "handshake")){
                memcpy(arre_ser[i_ser].ip, rec.obtieneDireccion(), 16);
                arre_ser[i_ser].puerto = rec.obtienePuerto();
                i_ser++;
                imprime_servidores();
            }else{
                if(msj[0] == 'C'){
                    memcpy(cliente.ip, rec.obtieneDireccion(), 16);
                    cliente.puerto = rec.obtienePuerto(); 

                    char numero[500];
					
                    unsigned long int n = strtoul(strncpy(numero, msj+1, strlen(msj)-1),&prt,10);
                    printf("Numero a procesar: %ld\n", n);
          //          unsigned long int rango = floor(n/servidores_length()); //SE DIVIDEN LOS SEGMENTOS
					unsigned long int rango =  1465702538;
                    for(i = 0; i < servidores_length(); i++){
                        PaqueteDatagrama env(lon);
                        env.inicializaPuerto(arre_ser[i].puerto);
                        env.inicializaIp(arre_ser[i].ip);
                        unsigned long int n_inicial;
                        unsigned long int n_final;
                        if(i+1 < servidores_length()){ //CUANDO SE PUEDE DIVIDIR BIEN
                            n_final = (i+1)*(rango)+1;  
                            n_inicial = n_final-(rango-1);
                        }else{                         //CUANDO ES IMPAR O NO SE PUEDE DIVIDIR EXACTO
                            n_final = n-1;  
                            n_inicial = (i)*(rango)+2;
                        }
                        //char msj[lon];
                        int aux = sprintf (msj, "%ld:%ld:%ld", n, n_inicial, n_final); //SE MANDA N, INICIAL Y FINAL
                        env.inicializaDatos(msj);
                        sd.envia(&env);
                    }
                }else{
                    if(msj[0] == 'S'){
                        char res[lon];
                        strncpy(res, msj+1, strlen(msj));
                        printf("Mensaje de servidor secundario: %s\n", msj);
						cout<<rec.obtieneDireccion()<<endl;
                        acumulador += atoi(res);
                        cuenta++;
                        if(cuenta == servidores_length()||(cuenta<servidores_length()&&acumulador>0)){
                            PaqueteDatagrama yn(lon);
                            yn.inicializaPuerto(cliente.puerto);
                            yn.inicializaIp(cliente.ip);
                            if(acumulador == 0){
                                strcpy(msj, "Es primo");
                            }else{
                                strcpy(msj, "No es primo");
                            }
                            yn.inicializaDatos(msj);
                            sd.envia(&yn);

                            //REINICIALIZAR
                            cuenta = 0;
                            acumulador = 0;
                        }
                    }
                }
            }
        }
    }
    if(tipo == 1){
        SocketDatagrama sd(puerto);
        PaqueteDatagrama env(lon);
        PaqueteDatagrama rec(lon);
        env.inicializaPuerto(7200);
        env.inicializaIp(ip_op);
        char msj[lon];
        strcpy(msj, "handshake");  
        env.inicializaDatos(msj);
        sd.envia(&env);

        while(1){
            sd.recibe(&rec);
            printf("Datos a procesar: %s\n", rec.obtieneDatos());
            char delim[] = ":";
            char *ptr = strtok(rec.obtieneDatos(), delim);
            unsigned long int n = strtoul(ptr,&prt,10);
            unsigned long int num1 = strtoul(strtok(NULL, delim),&prt,10);
            unsigned long int num2 = strtoul(strtok(NULL, delim),&prt,10);
            int boom = esPrimo(n, num1, num2);
            int aux = sprintf(msj, "S%d", boom);
            printf("Cálculo: %d\n", boom);
            env.inicializaDatos(msj);
            sd.envia(&env);
        }
    }
    		
	return 0;
}

void imprime_servidores(){
    int i;
    for(i = 0; i < servidores_length(); i++){
        printf("Servidor %d: %s: %d\n", i+1, arre_ser[i].ip, arre_ser[i].puerto);
    }
}

int servidores_length(){
    int cont = 0;
    while(arre_ser[cont].puerto != 0){
        cont++;
    }
    return cont;
}

int esPrimo(unsigned long int n, unsigned long int num1, unsigned long int num2){
    unsigned long int i;
    int boom = 0;
    for(i = num1; i <= num2; i++){
        if(n%i == 0){
            boom = 1; //DEVULEVE UN 1 SI NO ES PRIMO
			cout<<i<<endl;
			break;
        }
    }
    return boom;
}