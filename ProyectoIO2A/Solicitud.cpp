#include "Solicitud.h"
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include "mensaje.h"
#include <string.h>
#include <iostream>
using namespace std;
Solicitud::Solicitud(int pto){
	socketlocal = new SocketDatagrama(pto);
}

void Solicitud::sendTank(char *IP, int puerto, char *arguments){
	unsigned int lon = sizeof(Mensaje);
	PaqueteDatagrama env((char *)arguments, lon, IP, puerto);
	socketlocal->envia(&env);
}

char* Solicitud::receiveTanks(){
	unsigned int lon = sizeof(Mensaje);
    PaqueteDatagrama rec(lon);
    Mensaje *respuesta = (Mensaje *)malloc(lon);
    socketlocal->recibe(&rec);
    memcpy(respuesta, rec.obtieneDatos(), rec.obtieneLongitud());
    return (char *)respuesta;
}
