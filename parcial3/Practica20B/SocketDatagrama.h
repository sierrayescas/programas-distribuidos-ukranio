#ifndef SOCK_H_
#define SOCK_H_
#include "PaqueteDatagrama.h"
#include <netinet/in.h>
class SocketDatagrama{
	public:
	 	SocketDatagrama(int puertoVallarta);
	 	~SocketDatagrama();
	 	//Recibe un paquete tipo datagrama proveniente de este socket
	 	int recibe(PaqueteDatagrama *p);
	 	//Envía un paquete tipo datagrama desde este socket
	 	int envia(PaqueteDatagrama *p);
	 	//Hace lo mismo que recibe pero con ams cositas XD
	 	int recibeTimeout(PaqueteDatagrama *p, time_t segundos, suseconds_t microsegundos);
		int setBroadcast(int yes);
	private:
		struct sockaddr_in direccionLocal;
		struct sockaddr_in direccionForanea;
		int s; //ID socket
};
#endif