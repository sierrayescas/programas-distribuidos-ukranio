#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

#include <arpa/inet.h>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include <string.h>
using namespace std;

int puerto = 7200;

int main(void){
	unsigned int lon = 65505;
   char* msj = (char*)malloc(100*sizeof(char));

   strcpy(msj, "Amargo adios");

	SocketDatagrama *sd = new SocketDatagrama(puerto);
	PaqueteDatagrama *rec = new PaqueteDatagrama(lon);
   PaqueteDatagrama *env = new PaqueteDatagrama(lon);
	while(1){
		sd->recibe(rec);

      //IMPRIME MENSAJE DEL CLIENTE de LOL
      cout << "Mensaje recibido de: " << rec->obtieneDireccion() << endl;
      cout << "Puerto: " << rec->obtienePuerto() << endl;
      cout << "Mensaje del cliente: " << rec->obtieneDatos() << endl;

      env->inicializaIp(rec->obtieneDireccion());
      env->inicializaPuerto(rec->obtienePuerto());
      env->inicializaDatos(msj);

		sd->envia(env);
	}
   delete sd;
   delete rec;
   delete env;
   
	return 0;
}
