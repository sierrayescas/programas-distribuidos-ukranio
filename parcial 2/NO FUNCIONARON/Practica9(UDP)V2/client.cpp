#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

#include <arpa/inet.h>
#include "SocketDatagrama.h"
#include "PaqueteDatagrama.h"
#include <string.h>
using namespace std;

int puerto = 7777;

int main(int argc, char *argv[]){
	unsigned int lon = 65505;
	char* ip_op = argv[1];
	char* msj = (char*)malloc(100*sizeof(char));

	strcpy(msj, "Hola");

	SocketDatagrama sd(puerto);
	PaqueteDatagrama rec(lon), env(lon);
	env.inicializaDatos(msj);
	env.inicializaIp(ip_op);
	env.inicializaPuerto(7200);
	cout << "Mensaje enviado a server: " << env.obtieneDireccion() << endl;
	sd.envia(&env);
	sd.recibe(&rec);
	cout << "Puerto: " << ntohs(rec.obtienePuerto()) << endl;
	cout << "Respuesta del server: " << rec.obtieneDatos() << endl;

	return 0;
}
