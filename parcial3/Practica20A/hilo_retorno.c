#include <pthread.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include<time.h>
#define CDATOS 3500
int I=0;
struct cubeta{
	int cantidad;
	int inicio;
	int final;
	int *elementos;
}typedef cubeta;
void bubleshort(cubeta *cub);
int main (int argc, char *argv[])
{
cubeta *cub;
pthread_t *thread;
int datos[CDATOS];
int i,j,k;
int cCubetas=atoi(argv[1]);
int sobrante,rango;
srand (time(NULL));
//creacion de los hilos, hilo por cubeta
thread = (pthread_t*)malloc(sizeof(pthread_t)*cCubetas);
//creacion de los datos aleatorios
for(i=0;i<CDATOS;i++){
	datos[i]=rand()%1000;
	printf("%d\n",datos[i]);
}
//creacion cubetas
cub = (cubeta*)malloc(sizeof(cubeta)*cCubetas);
//rangos para las cubetas
rango = 1000/cCubetas;
for(i=0;i<cCubetas;i++){
	cub[i].cantidad=0;
	cub[i].inicio = i*rango;
	cub[i].final =((i+1)*rango)-1;
	if(i==cCubetas-1){
		cub[cCubetas-1].final = 999;
	}
	printf("Rango cubeta %d de %d a %d\n",i,cub[i].inicio,cub[i].final);
	//checa cuantos elementos hay en cada cubeta
	for(j=0;j<CDATOS;j++){
		if(datos[j]>=cub[i].inicio&&datos[j]<=cub[i].final){
			cub[i].cantidad+=1;
		}
	}
}
	cub[cCubetas-1].final = 999;
//creacion de espacio para las cubetas 
for(i=0;i<cCubetas;i++){
	cub[i].elementos=(int*)malloc(sizeof(int)*cub[i].cantidad);
	
	printf("La cubeta contiene %d elementos\n",cub[i].cantidad);
}
//asignacion de datos a las cubetas
for(j=0;j<cCubetas;j++){
	k=0;
	for(i=0;i<CDATOS;i++){
		if(datos[i]>=cub[j].inicio&&datos[i]<=cub[j].final){
			cub[j].elementos[k]=datos[i];
			//printf("\n%d",cub[j].elementos[k]);
			k++;
		}
	}
}
//uso d hilos 
for(i=0;i<cCubetas;i++){
	pthread_create(&thread[i], NULL, &bubleshort,&cub[i]);
}
for(i=0;i<cCubetas;i++){
	pthread_join(thread[i],NULL);
}
for(j=0;j<cCubetas;j++){
for(i=0;i<cub[j].cantidad;i++){
	printf("%d\n",cub[j].elementos[i]);
}
}
return 0;
}
void bubleshort(cubeta *cub){
	int aux=0;  
	int i,j;
	for(i=0;i<cub->cantidad;i++){
		for(j=1;j<cub->cantidad;j++){
			if(cub->elementos[i]<cub->elementos[j]){
				aux=cub->elementos[i];
				cub->elementos[i]=cub->elementos[j];
				cub->elementos[j]=aux;
			}
		}
	}
	printf("\n Cubeta con rango %d a %d termino de ordenar",cub->inicio,cub->final);
	
}